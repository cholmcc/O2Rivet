HOSTNAME	:= $(shell hostname)

ifeq ($(HOSTNAME),cholm-x1g9)
include Makefile.cholm
WHO		:= mine
else
include Makefile.alidist
WHO		:= other
endif

ROOT_LIBS	:= $(shell root-config --noldflags --noauxlibs --libs) -lEG
O2_LIBS		:= -lO2SimulationDataFormat   			\
		   -lO2CommonUtils				\
		   -lO2Framework				\
		   -lO2FrameworkFoundation			\
		   -lO2Headers					\
		   -lO2Steer					\
		   -lO2Mergers					\
		   -lO2Generators
O2AOD_LIBS	:= -lO2DetectorsVertexing			\
		   -lO2StrangenessTracking			\
		   -lO2DCAFitter				\
		   -lO2GlobalTrackingWorkflowHelpers		\
		   -lO2FT0Workflow				\
		   -lO2FT0Raw					\
		   -lO2FDDWorkflow				\
		   -lO2FDDReconstruction			\
		   -lO2FDDRaw					\
		   -lO2FV0Workflow				\
		   -lO2FV0Reconstruction			\
		   -lO2FV0Simulation				\
		   -lO2FV0Calibration				\
		   -lO2FV0Raw					\
		   -lO2FITRaw					\
		   -lO2GlobalTracking				\
		   -lO2FT0Reconstruction			\
		   -lO2FT0Simulation				\
		   -lO2TOFBase					\
		   -lO2TOFWorkflowUtils				\
		   -lO2TOFCalibration				\
		   -lO2TOFWorkflowIO				\
		   -lO2TOFReconstruction			\
		   -lO2HMPIDWorkflow				\
		   -lO2HMPIDReconstruction			\
		   -lO2HMPIDSimulation				\
		   -lO2ITSWorkflow				\
		   -lO2DetectorsDCS				\
		   -lO2GlobalTrackingWorkflowWriters		\
		   -lO2MCHTracking				\
		   -lO2MCHBase					\
		   -lO2MFTWorkflow				\
		   -lO2ITSMFTWorkflow				\
		   -lO2GlobalTrackingWorkflowReaders		\
		   -lO2MFTAssessment				\
		   -lO2MFTAlignment				\
		   -lO2MFTTracking				\
		   -lO2TPCWorkflow				\
		   -lO2GPUWorkflow				\
		   -lO2TPCSimulation				\
		   -lO2TPCQC					\
		   -lO2TPCCalibration				\
		   -lO2SpacePoints				\
		   -lO2TPCReconstruction			\
		   -lO2GPUO2Interface				\
		   -lO2GPUTracking				\
		   -lO2ITStracking				\
		   -lO2ITSReconstruction			\
		   -lO2Generators				\
		   -lO2ITSBase					\
		   -lO2GPUDataTypes				\
		   -lO2TRDBase					\
		   -lO2TPCReaderWorkflow			\
		   -lO2DataFormatsGlobalTracking		\
		   -lO2DataFormatsITS				\
		   -lO2DataFormatsFT0				\
		   -lO2FT0Base					\
		   -lO2DataFormatsFDD				\
		   -lO2FDDBase					\
		   -lO2DataFormatsFV0				\
		   -lO2DataFormatsFIT				\
		   -lO2FV0Base					\
		   -lO2DataFormatsTOF				\
		   -lO2DataFormatsHMP				\
		   -lO2HMPIDBase				\
		   -lO2DataFormatsTRD				\
		   -lO2DataFormatsMFT				\
		   -lO2MFTBase					\
		   -lO2ITSMFTReconstruction			\
		   -lO2DataFormatsITSMFT			\
		   -lO2ITSMFTBase				\
		   -lO2DataFormatsMCH				\
		   -lO2DataFormatsMID				\
		   -lO2DataFormatsZDC				\
		   -lO2DetectorsCalibration			\
		   -lO2DataFormatsEMCAL				\
		   -lO2DataFormatsCPV				\
		   -lO2CPVBase					\
		   -lO2DataFormatsPHOS				\
		   -lO2DetectorsBase				\
		   -lO2SimConfig				\
		   -lO2GPUDataTypeHeaders			\
		   -lO2TPCFastTransformation			\
		   -lO2TPCSpaceCharge				\
		   -lO2TPCBase					\
		   -lO2DataFormatsTPC				\
		   -lO2DataFormatsCalibration			\
		   -lO2DataFormatsCTP				\
		   -lO2DataFormatsParameters			\
		   -lO2ReconstructionDataFormats		\
		   -lO2CCDB					\
		   -lO2DetectorsCommonDataFormats		\
		   -lO2GPUUtils					\
		   -lO2Headers					\
		   -lO2MathUtils				\
		   -lO2ZDCBase				
FAIRROOT_LIBS	:= -lBase
FAIRLOGGER_LIBS	:= -lFairLogger
FMT_LIBS	:= -lfmt
ARROW_LIBS	:= -lgandiva					\
		   -larrow					
BOOST_LIBS	:= -lboost_container
HEPMC3_LIBS	:= -lHepMC3
RIVET_LIBS	:= $(filter -l%, $(shell rivet-config --libs))
PYTHIA_LIBS	:= -lpythia8

TARGETS		:= libO2PhysicsRivetOutput.so			\
		   rivet				
EGS		:= egs/pythia 					\
		   egs/angantyr					\
		   egs/tpythia					\
		   egs/readEvents				\
		   egs/readTEvents
KINE_PUB	:= o2-sim-kine-publisher
KINE_DEP	:=
AOD_DEP		:= 
AOD_PUB		:= o2-aod-mc-producer-workflow
ANALYSES	:= ALICE_YYYY_I1234567
ANA_DEP		:= analyses/ALICE_YYYY_I1234567.cc
OTH_DEP		:= 
OTH_PUB		:= o2-sim-mctracks-to-aod

LOG		:= 
ifdef DEBUG
LOG		:= --severity debug
endif

CPPFLAGS	:= -Io2				\
		   $(ROOT_CPPFLAGS)		\
		   $(O2_CPPFLAGS)		\
		   $(FAIRROOT_CPPFLAGS)		\
		   $(FAIRMQ_CPPFLAGS)		\
		   $(FAIRLOGGER_CPPFLAGS)	\
		   $(FMT_CPPFLAGS)		\
		   $(ARROW_CPPFLAGS)		\
		   $(MS_GSL_CPPFLAGS)		\
		   $(BOOST_CPPFLAGS)		\
		   $(UV_CPPFLAGS)		\
		   $(VMC_CPPFLAGS)		\
		   $(HEPMC3_CPPFLAGS)		\
		   $(RIVET_CPPFLAGS)		
CXX		:= g++ -c
CXXFLAGS	:= $(ROOT_CXXFLAGS) -g -fno-omit-frame-pointer


LD		:= g++
LDFLAGS		:= $(ROOT_LDFLAGS)		\
		   $(O2_LDFLAGS)		\
		   $(FAIRROOT_LDFLAGS)		\
		   $(FAIRMQ_LDFLAGS)		\
		   $(FAIRLOGGER_LDFLAGS)	\
		   $(FMT_LDFLAGS)		\
		   $(ARROW_LDFLAGS)		\
		   $(MS_GSL_LDFLAGS)		\
		   $(BOOST_LDFLAGS)		\
		   $(UV_LDFLAGS)		\
		   $(VMC_LDFLAGS)		\
		   $(HEPMC3_LDFLAGS)		\
		   $(RIVET_LDFLAGS)		\
		   $(CGAL_LDFLAGS)		
LIBS		:= $(ROOT_LIBS)			\
		   $(HEPMC3_LIBS)		\
		   $(RIVET_LIBS)		\
		   $(FAIRROOT_LIBS)		\
		   $(FAIRLOGGER_LIBS)		\
		   $(O2_LIBS)			\
		   $(FMT_LIBS)			\
		   $(ARROW_LIBS)		\
		   $(BOOST_LIBS)		\
		   $(RIVET_LIBS)
SO		:= g++ -shared
SOFLAGS		:= $(RIVET_LDFLAGS)
SOLIBS		:= $(RIVET_LIBS)


EG		:= pythia8pp
NEVENTS		:= 10
STOP		:=
BATCH		:= -b
NJOBS		:= 1
NTIMEFRAME	:= 1
ONLYGEN		:= yes
SIM_FLAGS	:= -j $(NJOBS)            				\
		   -n $(NEVENTS)   					\
		   $(if $(findstring yes,$(TRANSPORT)),,--noGeant) 	
RIVET_PROC      := 
RIVET_FLAGS	:= --rivet-analysis $(ANALYSES)				\
		   --rivet-pwd						\
		   --rivet-sources $(ANA_DEP)				\
		   --rivet-load-paths $(PWD)/analyses			\
		   --rivet-log INFO					\
		   --rivet-flags "$(CGAL_LDFLAGS)"			\
		   $(RIVET_PROC)
HEPMC_FLAGS	:= $(if $(findstring yes,$(ONLYGEN)),			\
			--hepmc-only-generated,)			\
		  --hepmc-recenter					\
		  --hepmc-use-tree

vpath AO2D_%.root keep

# O2_NO_CATCHALL_EXCEPTIONS
# SAL_DISABLE_OPENCL=1

%:%.o
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@

%.o:%.cxx
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $<

G__%.o:		CXXFLAGS:=$(CXXFLAGS) -fPIC

%_Kine.root:
	VMCWORKDIR=$(O2_ROOT)/share o2-sim  $(SIM_FLAGS) -g $* -o $*

gen%_Kine.root:gen%.hepmc
	VMCWORKDIR=$(O2_ROOT)/share 				\
	o2-sim  $(SIM_FLAGS)					\
		-g hepmc					\
		-o gen$*					\
		--configKeyValues "FileOrCmd.fileNames=$<;HepMC.version=3"

%.service:
	VMCWORKDIR=$(O2_ROOT)/share \
	o2-sim  $(SIM_FLAGS)					\
		-g $*						\
		-o $*						\
		--forwardKine					\
		--noDiscOutput > $*.log 2>&1 & 			\
	echo $$! > $@
	sleep .5

gen%.service:gen%.hepmc
	VMCWORKDIR=$(O2_ROOT)/share \
	o2-sim  $(SIM_FLAGS)					\
		-g hepmc					\
		-o gen$*					\
		--forwardKine					\
		--noDiscOutput 					\
		--configKeyValues "FileOrCmd.fileNames=$<;HepMC.version=3" \
		> gen$*.log 2>&1 & 				\
	echo $$! > $@
	sleep .5

%.kill:
	if test -f $*.service ; then 				\
		pid=`cat $*.service` ;				\
		kill -9 $$pid ; fi ;				\
	rm -f $*.service


AO2D_%.root:%_Kine.root $(AOD_DEP)
	$(AOD_PUB) 						\
		--mckine-fname $* 				\
		--aod-writer-resfile AO2D_$*	 		\
		--aod-writer-keep dangling 

%_Kine.hepmc:%_Kine.root $(AOD_DEP) o2-sim-mctracks-to-hepmc
	$(AOD_PUB) 						\
		--mckine-fname $* 			|	\
	./o2-sim-mctracks-to-hepmc $(BATCH) $(STOP) $(LOG) 	\
		--hepmc-dump $@					\
		$(HEPMC_FLAGS)					

%_Kine.yoda:%_Kine.root o2-analysis-mm-rivet $(AOD_DEP)
	$(AOD_PUB)		 				\
		--mckine-fname $* 			|	\
	./o2-analysis-mm-rivet $(BATCH) $(STOP) $(LOG) 		\
	 	--aod-writer-resfile $(*)_KineRivet		\
		--rivet-dump $@					\
		$(HEPMC_FLAGS)					\
		$(RIVET_FLAGS)
	mv AnalysisResults.root $(*)_KineRivet.root

%_KineRivet.root:%_Kine.yoda
	@if test ! -f $@ ; then $(MAKE) $(MAKEFLAGS) $< ; else : ; fi

AO2D_%.yoda:AO2D_%.root o2-analysis-mm-rivet  $(ANA_DEP)
	./o2-analysis-mm-rivet $(BATCH) $(STOP) $(LOG) 		\
		--aod-file $< 					\
		--aod-writer-resfile $(*)_AO2DRivet.root	\
		--rivet-dump $@					\
		$(HEPMC_FLAGS)					\
		$(RIVET_FLAGS)
	mv AnalysisResults.root $(*)_KineRivet.root

AO2D_%.hepmc:AO2D_%.root o2-sim-mctracks-to-hepmc
	./o2-sim-mctracks-to-hepmc $(BATCH) $(STOP) $(LOG) 	\
		--aod-file $< 					\
		$(HEPMC_FLAGS)					\
		--hepmc-dump $@					


%_service.yoda:%.service o2-analysis-mm-rivet $(OTH_DEP)  $(ANA_DEP)
	@echo "Service PID file '$<'"
	o2-sim-mctracks-proxy $(BATCH)				\
		--nevents $(NEVENTS)				\
		--o2sim-pid $(shell cat $<)			\
		--aggregate-timeframe  $(NTIMEFRAME)	|	\
	$(OTH_PUB) $(BATCH) $(LOG) |	 			\
	./o2-analysis-mm-rivet $(BATCH) $(STOP) $(LOG) 		\
		--aod-writer-resfile $(*)_KineRivet.root	\
		--rivet-dump $@					\
		$(HEPMC_FLAGS)					\
		$(RIVET_FLAGS)
	mv AnalysisResults.root $(*)_serviceRivet.root
	$(MAKE) $*.kill 

%_service.hepmc:%.service o2-sim-mctracks-to-hepmc $(OTH_DEP)  $(ANA_DEP)
	o2-sim-mctracks-proxy $(BATCH)				\
		--nevents $(NEVENTS)				\
		--o2sim-pid $(shell $<)				\
		--aggregate-timeframe  $(NTIMEFRAME)	|	\
	$(OTH_PUB) $(BATCH) $(LOG) |	 			\
	./o2-sim-mctracks-to-hepmc $(BATCH) $(STOP) $(LOG) 	\
		--hepmc-dump $@					\
		$(HEPMC_FLAGS)					
	$(MAKE) $*.kill 


all:	$(TARGETS)

clean:
	rm -f $(TARGETS) *~ *.o AnalysisResults.root runmc2hepmc
	rm -f old/*~ old/*.o *.gv *.gv.pdf *.so *.d *_h.* *.pcm G__*
	rm -f *.rootmap egs/*.o core*
	rm -f *merged_hits.root *geometry-aligned.root *geometry.root
	rm -f generatorFifo* egs/*~ 

cleancrap:
	rm -f *_Hits*.root *log *log0 *.json *.dat  *.ini
	rm -f *_[0-9][0-9]*.root
	rm -f *_grp*.root


realclean:clean cleancrap
	rm -f *_Kine.root *_MCHeader.root *.hepmc *.yoda
	rm -f AO2D_*.root $(EGS) *_KineRivet.root *_AO2DRivet.root

genpythia.hepmc:egs/pythia
	./$< -n $(NEVENTS) -o $@

genangantyr.hepmc:egs/angantyr
	./$< -n $(NEVENTS) -o $@ 

gencrmc.hepmc:egs/crmc
	./$< -n $(NEVENTS) >  $@


test1:	pythia8pp_Kine.yoda
test2:	pythia8pp_service.yoda

test_merge:genpythia_KineRivet.root 	\
	   gencrmc_KineRivet.root 	\
	   libO2PhysicsRivetOutput.so
	rm -f out.root
	hadd out.root $(filter %.root, $^)
	root -l -b -q WriteOutput.C\(\"out.root\",\"merged.yoda\"\)

show_%:%.yoda
	ipython3 -i analyses/ALICE_YYYY_I1234567.py $< 

o2-sim-mctracks-to-hepmc:	o2-sim-mctracks-to-hepmc.o
o2-sim-mctracks-to-hepmc.o:	o2-sim-mctracks-to-hepmc.cxx

rivet:				rivet.o
rivet:				libO2PhysicsRivetOutput.so
rivet:				LIBS:=$(LIBS) libO2PhysicsRivetOutput.so
rivet.o:			rivet.cxx	\
				RivetAOs.h	\
				Wrapper.h


egs/pythia.o:			egs/pythia.cc egs/Pythia.hh
egs/pythia.o:			CPPFLAGS=$(PYTHIA_CPPFLAGS) $(HEPMC3_CPPFLAGS)
egs/pythia:			egs/pythia.o
egs/pythia:			LDFLAGS=$(PYTHIA_LDFLAGS) $(HEPMC3_LDFLAGS)
egs/pythia:			LIBS=$(PYTHIA_LIBS)	  $(HEPMC3_LIBS)

egs/angantyr.o:			egs/angantyr.cc egs/Pythia.hh
egs/angantyr.o:			CPPFLAGS=$(PYTHIA_CPPFLAGS) $(HEPMC3_CPPFLAGS) 
egs/angantyr:			egs/angantyr.o
egs/angantyr:			LDFLAGS=$(PYTHIA_LDFLAGS) $(HEPMC3_LDFLAGS)
egs/angantyr:			LIBS=$(PYTHIA_LIBS)	  $(HEPMC3_LIBS)

egs/tpythia.o:			egs/tpythia.cc egs/Pythia.hh
egs/tpythia.o:			CPPFLAGS=$(PYTHIA_CPPFLAGS) $(HEPMC3_CPPFLAGS) $(ROOT_CPPFLAGS)
egs/tpythia:			egs/tpythia.o
egs/tpythia:			LDFLAGS=$(PYTHIA_LDFLAGS) $(HEPMC3_LDFLAGS) $(ROOT_LDFLAGS)
egs/tpythia:			LIBS=$(PYTHIA_LIBS)	  $(HEPMC3_LIBS) $(ROOT_LIBS)

egs/readEvents.o:		egs/readEvents.cc
egs/readEvent.o:		CPPFLAGS=$(HEPMC3_CPPFLAGS)
egs/readEvents:			egs/readEvents.o
egs/readEvents:			LDFLAGS=$(HEPMC3_LDFLAGS)
egs/readEvents:			LIBS=$(HEPMC3_LIBS)

egs/readTEvents.o:		egs/readTEvents.cc
egs/readTEvent.o:		CPPFLAGS=$(ROOT_CPPFLAGS)
egs/readTEvents:		egs/readTEvents.o
egs/readTEvents:		LDFLAGS=$(ROOT_LDFLAGS)
egs/readTEvents:		LIBS=$(ROOT_LIBS)

egs/testPrune.o:		egs/testPrune.cc
egs/testPrune.o:		CPPFLAGS=$(HEPMC3_CPPFLAGS)# -DPRUNE_BUG
egs/testPrune.o:		CXXFLAGS=-std=c++17 -g -pthread
egs/testPrune:			egs/testPrune.o
egs/testPrune:			LDFLAGS=$(HEPMC3_LDFLAGS)
egs/testPrune:			LIBS=$(HEPMC3_LIBS)

egs/testPrune2.o:		egs/testPrune2.cc
egs/testPrune2.o:		CPPFLAGS=$(HEPMC3_CPPFLAGS)# -DPRUNE_BUG
egs/testPrune2.o:		CXXFLAGS=-std=c++17 -g -pthread
egs/testPrune2:			egs/testPrune2.o
egs/testPrune2:			LDFLAGS=$(HEPMC3_LDFLAGS)
egs/testPrune2:			LIBS=$(HEPMC3_LIBS)

RivetAOs.o:			CXXFLAGS:=$(CXXFLAGS) -fPIC

G__%.cxx:		%LinkDef.h
	rootcling 						\
		--rmf=lib$*.rootmap				\
		--rml=$*					\
		-f $@						\
		$(CPPFLAGS)					\
		RivetAOs.h  $<

libO2Physics%.so:G__%.o RivetAOs.o
	$(SO) $(SOFLAGS) -o $@ $^ $(SOLIBS)

.PRECIOUS:	%_Kine.root gen%_Kine.root %.service gen%.service

dist:
	mkdir -p o2rivet
	cp rivet.cxx			\
	   Wrapper.h			\
	   RivetAOs.h			\
	   RivetAOs.cxx			\
	   RivetOutputLinkDef.h		\
	   GetRivetAOs.macro		\
	   Makefile			\
	   Makefile.cholm		\
	   Makefile.alidist		\
	   o2rivet/
	tar -czvf o2rivet.tar.gz o2rivet
	rm -rf o2rivet

distcheck:	dist
	mkdir -p tmp
	tar -xzvf o2rivet.tar.gz -C tmp
	$(MAKE) -C tmp/o2rivet/
	rm -rf tmp

show:
	@echo "ALIENV='$(ALIENV)'"
	@echo "WHO='$(WHO)'"
	@echo "ROOT_CPPFLAGS            = $(ROOT_CPPFLAGS)"
	@echo "O2_CPPFLAGS              = $(O2_CPPFLAGS)"
	@echo "VMC_CPPFLAGS             = $(VMC_CPPFLAGS)"
	@echo "FAIRMQ_CPPFLAGS          = $(FAIRMQ_CPPFLAGS)"
	@echo "FAIRROOT_CPPFLAGS        = $(FAIRROOT_CPPFLAGS)"
	@echo "FAIRLOGGER_CPPFLAGS      = $(FAIRLOGGER_CPPFLAGS)"
	@echo "FMT_CPPFLAGS             = $(FMT_CPPFLAGS)"
	@echo "ARROW_CPPFLAGS           = $(ARROW_CPPFLAGS)"
	@echo "MS_GSL_CPPFLAGS          = $(MS_GSL_CPPFLAGS)"
	@echo "BOOST_CPPFLAGS           = $(BOOST_CPPFLAGS)"
	@echo "UV_CPPFLAGS              = $(UV_CPPFLAGS)"
	@echo "HEPMC3_CPPFLAGS          = $(HEPMC3_CPPFLAGS)"
	@echo "RIVET_CPPFLAGS           = $(RIVET_CPPFLAGS)"
	@echo "PYTHIA_CPPFLAGS          = $(PYTHIA_CPPFLAGS)"
	@echo ""
	@echo "ROOT_LDFLAGS             = $(ROOT_LDFLAGS)"
	@echo "O2_LDFLAGS               = $(O2_LDFLAGS)"
	@echo "VMC_LDFLAGS              = $(VMC_LDFLAGS)"
	@echo "FAIRMQ_LDFLAGS           = $(FAIRMQ_LDFLAGS)"
	@echo "FAIRROOT_LDFLAGS         = $(FAIRROOT_LDFLAGS)"
	@echo "FAIRLOGGER_LDFLAGS       = $(FAIRLOGGER_LDFLAGS)"
	@echo "FMT_LDFLAGS              = $(FMT_LDFLAGS)"
	@echo "ARROW_LDFLAGS            = $(ARROW_LDFLAGS)"
	@echo "MS_GSL_LDFLAGS           = $(MS_GSL_LDFLAGS)"
	@echo "BOOST_LDFLAGS            = $(BOOST_LDFLAGS)"
	@echo "UV_LDFLAGS               = $(UV_LDFLAGS)"
	@echo "HEPMC3_LDFLAGS           = $(HEPMC3_LDFLAGS)"
	@echo "RIVET_LDFLAGS            = $(RIVET_LDFLAGS)"
	@echo "PYTHIA_LDFLAGS           = $(PYTHIA_LDFLAGS)"
	@echo "CGAL_LDFLAGS             = $(CGAL_LDFLAGS)"
	@echo ""
	@echo "ROOT_LIBS                = $(ROOT_LIBS)"
	@echo "O2_LIBS                  = $(O2_LIBS)"
	@echo "FAIRROOT_LIBS            = $(FAIRROOT_LIBS)"
	@echo "FAIRLOGGER_LIBS          = $(FAIRLOGGER_LIBS)"
	@echo "FMT_LIBS                 = $(FMT_LIBS)"
	@echo "ARROW_LIBS               = $(ARROW_LIBS)"
	@echo "BOOST_LIBS               = $(BOOST_LIBS)"
	@echo "HEPMC3_LIBS              = $(HEPMC3_LIBS)"
	@echo "RIVET_LIBS               = $(RIVET_LIBS)"
	@echo "PYTHIA_LIBS              = $(PYTHIA_LIBS)"
	@echo ""
	@echo "CPPFLAGS                 = $(CPPFLAGS)"
	@echo "LDFLAGS                  = $(LDFLAGS)"

