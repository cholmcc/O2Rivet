// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Config/RivetConfig.hh"
#include "Rivet/Projections/AliceCommon.hh"

namespace Rivet
{
  /** @brief Example analysis */
  class ALICE_YYYY_I1234567 : public Analysis
  {
  public:
    typedef ALICE::PrimaryParticles PrimProj; // Need - Rivet 3
    
    /** Constructor */
    ALICE_YYYY_I1234567() : Analysis("ALICE_YYYY_I1234567") {}

    /**
     * @name Analysis methods
     * @{
     */
    /** Book histograms and initialise projections before the run */
    void init()
    {
      // Initialise and register projections
      declare(PrimProj(Cuts::abseta < 5 && Cuts::abscharge3 > 0),"APRIM");
      // book histograms 
      book(_h, 1, 1, 1);
    }
    /** Perform the per-event analysis */
    void analyze(const Event& event)
    {
      for (auto p : apply<PrimProj>(event,"APRIM").particles())
	_h->fill(p.eta());
    }
    /** Normalise histograms etc., after the run */
    void finalize()
    {
      std::cout << "Scaling histogram by " << numEvents()
		<< " " << sumW() << std::endl;
      scale(_h, 1./sumW());
    }
    /** @} */

    /**
     * @name Histograms
     * @{
     */
    Histo1DPtr _h;
    /* @} */
  };
  RIVET_DECLARE_PLUGIN(ALICE_YYYY_I1234567);
}

//
// EOF
//
