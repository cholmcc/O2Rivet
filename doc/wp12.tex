\pdfobjcompresslevel=1
\pdfminorversion=4
% --- Class, packages and theme --------------------------------------
\documentclass[compress,table,8pt]{beamer}
\usetheme[nobuttons]{alice}
\setbeamertemplate{navigation symbols}{}
\usepackage[export]{adjustbox}%%
\usepackage{multirow}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes}
\usetikzlibrary{calc}
\usetikzlibrary{fit}

\newcommand\Rivet{{\scshape Rivet}}
\newcommand\Yoda{{\scshape Yoda}}
\newcommand\HepMC{{\scshape HepMC}}
\newcommand\Otwo{O\textsuperscript{2}}
\newcommand\AOD{AO\textsuperscript{2}D}
\newcommand\CRMC{{\scshape Crmc}}
\newcommand\meta[1]{$\left\langle\text{\itshape\ttfamily #1}\right\rangle$}

%% --- Title, and so on ----------------------------------------------
\title{\Otwo{} Generator Upgrades}%
\subtitle{PR: \url{https://github.com/AliceO2Group/AliceO2/pull/11913}}
\author[C.H.Christensen]{Christian Holm Christensen}
\institute{\inst{1}Niels Bohr Institute}
\date[27.~Sep, 2023]{27\textsuperscript{th} of September, 2023}
\subject{Rivet}
\titlepageextra{%
  \begin{flushright}
    \includegraphics[width=\linewidth]{o2}
  \end{flushright}}
%% === The document ==================================================
\begin{document}
%% --- Title slide ---------------------------------------------------
\aliceTitlePage

\begin{frame}
  \Overview{}
\end{frame}

\begin{frame}
  \frametitle{Predefined MC header keys}

  In \texttt{MCEventHeader}
  
  \begin{columns}[onlytextwidth,t]
    \begin{column}{.6\linewidth}
      \begin{itemize}
      \item<+-> Define standardised key names
        \begin{itemize}
        \item To be used when writing and reading
        \item Avoids confusion
        \item Promotes filling in information
        \end{itemize}
      \item<+-> Keys for \HepMC{} compliant info
        \begin{itemize}
        \item For building \HepMC{} event structure to pass to Rivet
        \item<+->  \Otwo{}\Rivet{} project at
          \url{https://gitlab.com/cholmcc/O2Rivet}
        \end{itemize}
      \end{itemize}

      \begin{alertblock}<+->{In fact\textellipsis}
        \textellipsis all these changes are for the benefit of the
        \Rivet{} integration into \Otwo{}
      \end{alertblock}
    \end{column}
    \begin{column}{.35\linewidth}
      \uncover<3->{
        \includegraphics[width=\linewidth]{o2rivet}}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Full \HepMC{} event record}

  In \texttt{GeneratorHepMC} and \texttt{GeneratorPythia8}
  
  \begin{columns}[onlytextwidth,t]
    \begin{column}{.45\linewidth}
      \begin{block}<+->{Was}
        \begin{itemize}
        \item Only most basic information $b$, or 
        \item not \HepMC{} compliant information 
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.45\linewidth}
      \begin{block}<+->{Now}
        \begin{itemize}
        \item $\sigma$ 
        \item PDF parameters 
        \item As much Heavy-ion ``geometry'' info as available in
          \HepMC{} compliant fashion\newline
          {\footnotesize $b$, $N_{\mathrm{part}}$,
            $N_{\mathrm{coll}}$, $\varepsilon$} 
        \end{itemize}
      \end{block}
      
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Spawn child EG process}

  In \texttt{GeneratorHepMC}

  \begin{columns}[onlytextwidth,t]
    \begin{column}{.45\linewidth}
      \begin{block}<+->{Process}
        \begin{itemize}
        \item Make FIFO
        \item Spawn \meta{command-line}
        \item Read \HepMC{} events from FIFO
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.45\linewidth}
      \begin{block}<+->{Requirements}
        \begin{itemize}
        \item \meta{command-line} \emph{must} write \HepMC{} events on
          \texttt{stdout} 
          \begin{itemize}
          \item Other output on \texttt{stdout} \emph{not}
            allowed
          \end{itemize}
        \item \meta{command-line} \emph{must} accept option
          \texttt{-n}~\meta{n-events}. 
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
  \begin{block}<+->{Filter output}
    \begingroup\ttfamily
    \meta{command-line} -n \meta{n-events} \textbar{}
    sed -n 's/\^{}\textbackslash(HepMC::\textbackslash\textbar
    [EAUWVP]\textvisiblespace\textbackslash)/\textbackslash1/p'
    \endgroup
  \end{block}
  \setbeamercolor{block body example}{bg=gray!25!white}
  \begin{exampleblock}<+->{For example, \CRMC{} script \texttt{crmc.sh}}
\begin{verbatim}
#!/bin/sh

crmc $@ -o hepmc3 -f /dev/stdout | \
    sed -n 's/^\(HepMC::\|[EAUWVP] \)/\1/p'
\end{verbatim}
  \end{exampleblock}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Set up with child process}
  \begin{itemize}
  \item<+-> Configure via option \texttt{--configKeyValues}
    \begin{itemize}
    \item \texttt{HepMC.progCmd=}\meta{command-line}
    \end{itemize}
  \item<+-> Option \texttt{--nEvents} \meta{n-events} propagated
  \end{itemize}

  \setbeamercolor{block body example}{bg=gray!25!white}
  \begin{exampleblock}<+->{Example: \CRMC{} simulation}
\begin{verbatim}
    VMCWORKDIR=${O2_ROOT}/share o2-sim -j 10 -n 1000 -g hepmc \
       -o dpmjet_pPb --configKeyValues "HepMC.progCmd=crmc.sh \
           -c ${CRMC_ROOT}/etc/crmc.param \
           -i 2212 -I 1002080820 -m 12;HepMC.version=3"
\end{verbatim}
  \end{exampleblock}

  \begin{itemize}
  \item<+-> \emph{Any} EG with \HepMC{} output can be used
    \begin{itemize}
    \item \CRMC{} give \textsc{Hijing}, \textsc{Epos-Lhc},
      \textsc{DpmJET}, \textellipsis 
    \item \textsc{JetScape}, \textsc{Smash}, \textsc{Herwig}, \textellipsis
    \end{itemize} \textsc{Ampt} easily wrapped 
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{New generator: \texttt{GeneratorTParticle}}

  \begin{columns}[onlytextwidth,t]
    \begin{column}{.45\linewidth}
      \begin{block}{Features}
        \begin{itemize}
        \item<+-> Reads \texttt{TParticle} objects in a
          \texttt{TClonesArray} branch of a \texttt{TChain}.
        \item<+-> Can \emph{also} spawn \meta{command-line} \textit{a la}
          \texttt{GeneratorHepMC} 
        \item<+-> Names configurable via \texttt{--configKeyValues}
          \begin{itemize}
          \item \texttt{TParticle.fileName} for the file name, \emph{or}
          \item \texttt{TParticle.progCmd} for the \meta{command-line}
          \item \texttt{TParticle.treeName} for the \texttt{TTree} name
          \item \texttt{TParticle.branchName} for the
            \texttt{TClonesArray} branch name
          \end{itemize}
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.45\linewidth}
      \begin{block}<+->{Requirements of \meta{command-line}}
        \begin{itemize}
        \item \meta{command-line} \emph{must} accept option
          \texttt{-n}~\meta{file-name} 
        \item \meta{command-line} \emph{must} accept option
          \texttt{-n}~\meta{n-events}. 
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{To come \textellipsis}

  \begin{columns}
    \begin{column}{.45\linewidth}
      \begin{block}{\Otwo{} proper}
        \begin{itemize}[<+->]
        \item Improvement to \texttt{o2-aod-mc-producer-workflow}
          which \emph{also} publish \HepMC{} auxiliary tables (from
          \texttt{MCEventHeader} info).\newline
          {\footnotesize possibly optional}
        \item Unification of code of
          \texttt{o2-aod-mc-producer-workflow} and
          \texttt{o2-sim-mctracks-to-aod}
        \item Fix to \texttt{o2-sim-kine-publisher}\newline
          {\footnotesize currently publish only first event, and
            particle indexes messed up}
        \item Refactor \texttt{GeneratorHepMC} and
          \texttt{GeneratorTParticle} to use common, secondary, base
          class \texttt{GeneratorFileOrCmd}.
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.45\linewidth}
      \begin{block}<+->{For \Rivet{} integration}
        \begin{itemize}[<+->]
        \item Q: Should \Otwo{}\Rivet{} be standalone, in \Otwo{} or
          \Otwo{}Physics? 
        \item Deploy \HepMC{}-3 on platforms 
        \item Deploy \Yoda{} and \Rivet{} on platforms\newline
          {\footnotesize Build against \HepMC{}-3}
        \item Deploy \Otwo{}\Rivet{}
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
  \begin{block}<+->{\Otwo\Rivet{} status}
    \begin{itemize}
    \item<+-> Mostly done 
    \item<+-> Ensure that merging and \texttt{finalize} works
      \begin{itemize}
      \item<+-> \textsc{AliPhysics} had \texttt{Terminate} step to run
        \texttt{AliAnalysisTask} finalisation 
      \item<+-> Q: Does \Otwo{} have that too? 
      \item<+-> Q: How to set up a test of that?  
      \end{itemize}
    \end{itemize}
    
  \end{block}
\end{frame}
\end{document}
%%
%% EOF
%%
