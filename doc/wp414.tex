\pdfobjcompresslevel=1
\pdfminorversion=4
% --- Class, packages and theme --------------------------------------
\documentclass[compress,table,10pt]{beamer}
\usetheme[nobuttons]{alice}
\setbeamertemplate{navigation symbols}{}
\usepackage[export]{adjustbox}%%
\usepackage{multirow}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes}
\usetikzlibrary{calc}
\usetikzlibrary{fit}

\newcommand\Alice{{\scshape Alice}}
\newcommand\Rivet{{\scshape Rivet}}
\newcommand\Yoda{{\scshape Yoda}}
\newcommand\FastJet{{\scshape FastJet}}
\newcommand\FastJetContrib{\FastJet{}+contrib}
\newcommand\Python{{\scshape Python}}
\newcommand\HepMC{{\scshape HepMC3}}
\newcommand\HepData{{\scshape HepData}}
\newcommand\Otwo{O\textsuperscript{2}}
\newcommand\AOD{AO\textsuperscript{2}D}
\newcommand\CRMC{{\scshape Crmc}}
\newcommand\meta[1]{$\left\langle\text{\itshape\ttfamily #1}\right\rangle$}
\newcommand\OtwoRivet{\Otwo{}\Rivet}
\newcommand\OtwoPhysics{\Otwo{}{\scshape Physics}}
\newcommand\alidist{\texttt{alidist}}
  
\colorlet{NoGo}{red!75!black}
\colorlet{Check}{green!75!black}
\def\Check{{\color{Check}\ding{52}}}
\def\NoGo{{\color{NoGo}\ding{56}}}

%%
%% See https://tex.stackexchange.com/questions/6135/how-to-make-beamer-overlays-with-tikz-node
\tikzset{
  only/.code args={<#1>#2}{%
    \only<#1>{\pgfkeysalso{#2}} 
  },
  hide/.style={
    opacity=0,
  },
  show/.style={
    opacity=1
  },
  data/.style={
    shape=ellipse,
    fill=aliceyellow!50!white,
    text=alicegray,
    font=\bfseries\large,
    draw=alicegray
  },
  proc/.style={
    shape=rectangle,
    fill=alicered!50!white,
    text=alicegray,
    font=\bfseries\large,
    draw=alicegray,
  },
  code/.style={
    font=\ttfamily\footnotesize},
  con/.style={
    ->,
    line width=1pt,
    draw=alicegray
  }
}

\hypersetup{colorlinks=true,linkcolor=alicegray,urlcolor=alicepurple}
%% --- Title, and so on ----------------------------------------------
\title{\OtwoRivet{}}%
\subtitle{Integration of \Rivet{} into \Otwo{}/\OtwoPhysics{}}
\author[C.H.Christensen]{Christian Holm Christensen}
\institute{\inst{1}Niels Bohr Institute}
\date[29.~Jan., 2024]{29\textsuperscript{th} of January, 2024}
\place[WP4+14]{\Otwo{} workpackages 4~\&~14}
\subject{Rivet}
\titlepageextra{%
  \begin{flushright}
    \includegraphics[width=.7\linewidth]{o2rivet}\newline
    \href{https://indico.cern.ch/event/1334550/\#93-development-rivet-in-o2}{Presentation
      of project}
  \end{flushright}}
%% === The document ==================================================
\begin{document}
%% --- Title slide ---------------------------------------------------
\aliceTitlePage

\begin{frame}
  \Overview{}
\end{frame}

%% --- Goals ---------------------------------------------------------
\section{Motivation, goals, implementation}
\begin{frame}
  \frametitle{\Rivet{} is for \textellipsis}

  \begin{itemize}[<+->]
  \item \emph{Consistent} and \emph{robust} model to data comparison
  \item Model tuning 
  \item \emph{Analysis} of model data 
  \end{itemize}

  \begin{columns}[t,onlytextwidth]
    \begin{column}{.48\linewidth}
      \begin{block}<+->{What is it to \Alice{}}
        \begin{itemize}
        \item Comparisons in publications
          \begin{itemize}
          \item Use official productions for sanctioned model settings
          \end{itemize}
        \item Document how model analysis was done
          \begin{itemize}
          \item Often not documented in analysis notes
          \end{itemize}
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.48\linewidth}
      \begin{block}<+->{What is needed from \Alice{}}
        \footnotesize
        \begin{itemize}
        \item Every result ideally accompanied by \emph{validated}
          \Rivet{} analysis
          \begin{itemize}
          \item Lower the threshold for implementing these
          \end{itemize}
        \item Validation, EG results often need large model sample
          \begin{itemize}
          \item Impractical for user, re-use productions 
          \end{itemize}
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
  \begin{center}
    \uncover<+->{{\large\alert{\OtwoRivet{} addresses these issues}}}
  \end{center}
  
\end{frame}

\begin{frame}
  \frametitle{Overall goals}

  {\Large
    \begin{itemize}[<+->]
    \item Encourage use of \Rivet{} in \Alice{}\\[-.7ex]
      {\footnotesize \emph{Consistent} robust comparisons}
    \item Same interface as other \Alice{} analysis interfaces\\[-.7ex]
      {\footnotesize Lower the bar for adoption}
    \item Re-use official simulation productions\\[-.7ex]
      {\footnotesize \emph{Crucial} for sample size, and officially
        sanctioned model tunes} 
    \item Consistent comparison of models to data\\[-.7ex]
      {\footnotesize \emph{Directly} impacts physics results}
    \item Model ``validations''\\[-.7ex]
      {\footnotesize Check models for \emph{many} predictions}
    \item Documentation of analyses for outside \Alice{}\\[-.7ex]
      {\footnotesize Help model-builders in tuning, missing from
        analysis notes}
    \end{itemize}
  }
\end{frame}

%% -------------------------------------------------------------------
\begin{frame}
  \frametitle{What \OtwoRivet{} does}
  \begin{columns}
    \begin{column}{.6\linewidth}
      \begin{itemize}
      \item<1-> Read in \AOD{}
      \end{itemize}
      \begin{itemize}
      \item<2-> Convert to \HepMC{} event structure
      \end{itemize}
      \begin{itemize}
      \item<3-> Run \Rivet{} on \HepMC{} event structure
      \end{itemize}
      \begin{itemize}
      \item<4-> Store \Yoda{} output
      \end{itemize}
    \end{column}
    \begin{column}{.35\linewidth}
      \begin{center}
        \begin{tikzpicture}
          \node[data,hide,only={<1->{show}}] (aod) {\AOD};
          \node[proc,hide,only={<2->{show}},below=6mm of aod.south] (cnv) {Convert};
          \node[data,hide,only={<2->{show}},below=6mm of cnv.south] (evt) {\HepMC};
          \node[proc,hide,only={<3->{show}},below=6mm of evt.south] (rvt) {\Rivet};
          \node[data,hide,only={<4->{show}},below=6mm of rvt.south] (yda) {\Yoda};
          \draw[con,hide,only={<2->{show}}] (aod)--(cnv);
          \draw[con,hide,only={<2->{show}}] (cnv)--(evt);
          \draw[con,hide,only={<3->{show}}] (evt)--(rvt);
          \draw[con,hide,only={<4->{show}}] (rvt)--(yda);
        \end{tikzpicture}
      \end{center}
    \end{column}
  \end{columns}
  \uncover<5->{See also
    \href{https://indico.cern.ch/event/1334550/\#93-development-rivet-in-o2}{presentation
    of project}} 
\end{frame}

%% -------------------------------------------------------------------
\begin{frame}
  \frametitle{Steps to get there}

  \begin{itemize}[<+->]
  \item[$\square$\only<+->{\kern-.75em\Check}] Full generator headers\newline
    {\footnotesize \uncover<.->{\Otwo{} merge request
        \href{https://github.com/AliceO2Group/AliceO2/pull/12032}{\texttt{\#12032}}}}
  \item[$\square$\only<+->{\kern-.75em\Check}] All \HepMC{} info
    propagated to \AOD{}s\newline
    {\footnotesize \uncover<.->{\Otwo{} merge request
        \href{https://github.com/AliceO2Group/AliceO2/pull/12039}{\texttt{\#12039}}}}
  \item[$\phantom{(}\square\phantom{)}$\only<+->{\kern-1.5em(\Check\!)}] Deploy \Rivet{} on
    CVMFS\newline
    {\footnotesize \uncover<.->{\alidist{} merge request
        \href{https://github.com/alisw/alidist/pull/5320}{\texttt{\#5320}}}}
  \item[$\square$\only<+->{\kern-.75em\Check}] Develop \OtwoRivet{}
    ``task''\newline
    {\footnotesize \uncover<.->{\OtwoRivet{}
        \href{https://gitlab.com/cholmcc/O2Rivet}{project} and \OtwoPhysics{} merge request
        \href{https://github.com/AliceO2Group/O2Physics/pull/4400}{\texttt{\#4400}}}}
  \item[$\square$\only<+->{\kern-.75em\NoGo}] Deploy \OtwoRivet{} to
    end-users.
  \end{itemize}

  \begin{actionenv}<.-|@alert+>
    \begin{center} {\large \alert{Topic of today's discussion}}
    \end{center}
  \end{actionenv}

  
\end{frame}

\begin{frame}
  \frametitle{Goals for user-deployment}

  {\LARGE
    \begin{itemize}[<+->]
    \item \alert{Make it as easy as possible for the end-user to use \Rivet{}}
    \end{itemize}
  }

  \begin{itemize}[<+->]
  \item Be as close to other work-flows as possible 
  \item Seamless integration to read simulation \AOD{}s 
  \item Integrate with regular analysis flow to promote clear and
    robust model to data comparisons 
  \item Minimal impact on user environment 
  \item Minimal impact on maintenance 
  \end{itemize}
\end{frame}

%% --- Goals ---------------------------------------------------------
\section{User environments and deployment options}
\begin{frame}
  \frametitle{User environments}

  \begin{columns}[t,onlytextwidth]
    \begin{column}{.48\linewidth}
      \begin{tabular}[t]{p{.17\linewidth}p{.8\linewidth}}
        \uncover<+->{\includegraphics[width=\linewidth,valign=t]{computer}}
        & \uncover<.->{Own computer
          \begin{itemize}
          \item \emph{Very} limited data access
          \item Development oriented 
          \end{itemize}
          }
        \\
        \uncover<+->{\includegraphics[width=\linewidth,valign=t]{cvmfs}}
        & \uncover<.->{CVMFS
          \begin{itemize}
          \item Limited data access 
          \item Development oriented 
          \item Limited analysis
          \end{itemize}
          }
        \\
      \end{tabular}
    \end{column}
    \begin{column}{.48\linewidth}
      \begin{tabular}[t]{p{.17\linewidth}p{.8\linewidth}}
        \uncover<+->{\includegraphics[width=\linewidth,valign=t]{grid}}
        & \uncover<.->{Grid
          \begin{itemize}
          \item Full data access
          \item Limited analysis 
          \end{itemize}
          }
        \\
        \uncover<+->{\includegraphics[width=\linewidth,valign=t]{hyperloop}}
        & \uncover<.->{Hyper-loop
          \begin{itemize}
          \item Full data access
          \item Full analysis 
          \end{itemize}
          }
        \\
      \end{tabular}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Deployment target options}

  \begin{columns}[t,onlytextwidth]
    \begin{column}{.54\linewidth}
      \begin{block}{Where \OtwoRivet{} can go}
        
        \begin{tabular}[]{p{.2\linewidth}p{.75\linewidth}}
        \uncover<+->{\includegraphics[width=\linewidth,valign=t]{o2rivet}}
          & \uncover<.->{\OtwoRivet\newline
            As standalone package}
          \\
          \uncover<+->{\includegraphics[width=\linewidth,valign=t]{o2}}
          & \uncover<.->{\Otwo\newline
            Integrate as application}
          \\
          \uncover<+->{\includegraphics[width=\linewidth,valign=t]{o2physics}}
          & \uncover<.->{\OtwoPhysics\newline
            Integrate as application}
          \\
        \end{tabular}
      \end{block}
    \end{column}
    \begin{column}{.43\linewidth}
      \begin{block}<+->{Considerations in all cases}
        \begin{itemize}[<+->]
        \item Dependencies
          \begin{itemize}[<.->]
            \item \Rivet{}
              \begin{itemize}[<.->]
              \item \Yoda{}
              \item \HepMC{}
              \item \FastJetContrib{}
              \item \Python{}
              \end{itemize}
            \end{itemize}
          \item Maintainability
          \begin{itemize}[<.->]
          \item Single application
          \end{itemize}
        \item Over-all strategy 
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

%% --- Goals ---------------------------------------------------------
\def\intitle#1{%
  \begin{picture}(0,0)%
    \put(0,10){\includegraphics[width=2em,valign=t]{#1}}%
  \end{picture}\hspace*{2.2em}}
\section{Pros and cons of options}
\begin{frame}
  \frametitle{\intitle{o2rivet} As standalone}
  \begin{columns}[t,onlytextwidth]
    \begin{column}{.48\linewidth}
      \begin{block}{Pros}
        \small
        \begin{itemize}[<+->]
        \item No impact on \Otwo{} nor \OtwoPhysics{}
        \item Install on-demand on own computer
        \end{itemize}
      \end{block}
      \begin{block}<.(3)->{Responsibilities}
        \small
        PWGMM/MC and \alidist{}
      \end{block}
    \end{column}
    \begin{column}{.48\linewidth}
      \begin{block}{Cons}
        \small
        \begin{itemize}[<+->]
        \item No Hyper-loop
          \begin{itemize}[<.->]
          \item Non-standard usage pattern --- lower adoption
          \item Non-direct data access --- less consistent comparisons 
          \end{itemize}
        \item Grid package synchronisation
          \begin{itemize}[<.->]
          \item Maintenance hurdle 
          \end{itemize}
        \item CVMFS synchronisation
          \begin{itemize}[<.->]
          \item Maintenance hurdle 
          \item Not for large-scale analyses 
          \end{itemize}
        \end{itemize}
      \end{block}      
    \end{column}
  \end{columns}

\end{frame}

\begin{frame}
  \frametitle{\intitle{o2}In \Otwo{}}
  \begin{columns}[t,onlytextwidth]
    \begin{column}{.48\linewidth}
      \small
      \begin{block}{Pros}
        \begin{itemize}[<+->]
        \item Grid package synchronisation
          \begin{itemize}[<.->]
          \item When \Otwo{} upgraded
          \end{itemize}
        \item CVMFS synchronisation
          \begin{itemize}[<.->]
          \item When \Otwo{} upgraded
          \end{itemize}
        \end{itemize}
      \end{block}
      \begin{block}<.(4)->{Responsibilities}
        \Otwo{}, PWGMM/MC, and \alidist{}
      \end{block}
    \end{column}
    \begin{column}{.48\linewidth}
      \small
      \begin{block}{Cons}
        \begin{itemize}[<+->]
        \item No Hyper-loop
          \begin{itemize}[<.->]
          \item Non-Standard usage pattern --- lower adoption
          \item Non-direct data access --- less consistent comparisons
          \end{itemize}
        \item Additional dependencies
          \begin{itemize}[<.->]
          \item For user-installation optional 
          \item \Rivet{}
            \begin{itemize}
            \item \Yoda{}
            \item Other dependencies already covered by \Otwo{}
            \end{itemize}
          \end{itemize}
        \item \Otwo{} not for analysis
        \end{itemize}
      \end{block}      
    \end{column}
  \end{columns}

\end{frame}

\begin{frame}
  \frametitle{\intitle{o2physics}In \OtwoPhysics{}}
  \begin{columns}[t,onlytextwidth]
    \begin{column}{.48\linewidth}
      \small
      \begin{block}{Pros}
        \begin{itemize}[<+->]
        \item Hyper-loop
          \begin{itemize}[<.->]
          \item Standard usage pattern --- ease of use
          \item Direct data access --- consistent comparisons 
          \end{itemize}
        \item Grid package synchronisation
          \begin{itemize}[<.->]
          \item When \OtwoPhysics{} upgraded
          \end{itemize}
        \item CVMFS synchronisation
          \begin{itemize}[<.->]
          \item When \OtwoPhysics{} upgraded
          \end{itemize}
        \item \OtwoPhysics{} for analyses
          \begin{itemize}[<.->]
          \item \Rivet{} analyses arguably analyses like others
          \end{itemize}
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.48\linewidth}
      \small
      \begin{block}{Cons}
        \begin{itemize}[<+->]
        \item Additional dependencies
          \begin{itemize}[<.->]
          \item For user-installation optional 
          \item \Rivet{}
            \begin{itemize}
            \item \Yoda{}
            \item Other dependencies already covered by \Otwo{}
            \end{itemize}
          \end{itemize}
        \end{itemize}
      \end{block}      
      \begin{block}<+->{Responsibilities}
        \OtwoPhysics{}, PWGMM/MC, and \alidist{}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

%% --- Goals ---------------------------------------------------------
\section{Summary}
\begin{frame}
  \frametitle{Summarised}

  \begin{center}
    \footnotesize
    \begin{tabular}{|l|ccc|c|}
        % p{.15\linewidth}|p{.15\linewidth}|p{.15\linewidth}|
      \hline
      \headrowcolor{}%
      \textbf{Consideration}
      & \includegraphics[width=.1\linewidth,valign=m]{o2rivet} 
      & \includegraphics[width=.1\linewidth,valign=m]{o2} 
      & \includegraphics[width=.1\linewidth,valign=m]{o2physics}
      & 
      \\
      \headrowcolor{}%
      \textbf{User}
      & Standalone
      & \Otwo{}
      & \OtwoPhysics
      & 
      \\
      \hline 
      ``Standard workflow''
      & \NoGo{}
      & \NoGo{}
      & \Check{}
      &
      \\
      \altrowcolor{}%
      Data access
      & \NoGo{}
      & (\NoGo{})
      & \Check{}
      & \cellcolor{white}
      \\
      Analysis integration
      & \NoGo{}
      & \NoGo{}
      & \Check{}
      & \multirow{-3}*{\includegraphics[width=.1\linewidth]{hyperloop}}
      \\
      \altrowcolor{}%
      Low impact
      & \Check{}
      & \Check{}
      & \Check{}
      & Optional dep.
      \\
      \hline
      \headrowcolor{}%
      \textbf{System}
      & Standalone
      & \Otwo{}
      & \OtwoPhysics
      & 
      \\
      \hline
      Dependencies
      & \Check
      & \NoGo
      & \NoGo
      & \Yoda{}, \Rivet{}
      \\
      \altrowcolor{}%
      CVMFS sync.
      & \NoGo{}
      & \Check{}
      & \Check{}
      & 
      \\
      Grid sync.
      & \NoGo{}
      & \Check{}
      & \Check{}
      & \altcellcolor\multirow{-2}*{\alidist{}}
      \\
      \altrowcolor{}%
      Strategy
      & (\Check{})
      & \NoGo{}
      & \Check{}
      & \AOD{} Analysis
      \\
      \hline
    \end{tabular}
  \end{center}


  \begin{alertblock}{Recommendation}
    Integrate into \OtwoPhysics{}
  \end{alertblock}
\end{frame}

%% --- Goals ---------------------------------------------------------
% \section{Back-up}
\begin{frame}
  \vfill
  \begin{center}
    {\Huge\textbf{Back-up}}
  \end{center}
  \vfill
\end{frame}

%% -------------------------------------------------------------------
\begin{frame}
  \frametitle{Dependencies}

  \begin{itemize}
  \item \Rivet{} optional dependency for user install 
  \item For CVMFS, Grid, and Hyperloop installation \Rivet{} should be
    enabled
    \begin{itemize}
    \item Standard workflow for users 
    \end{itemize}
  \item \Rivet{} depends on
    \begin{itemize}
    \item \Yoda{}, which depends on
      \begin{itemize}
      \item \Python{} (already a dependency of \Otwo{})
      \end{itemize}
    \item \FastJetContrib{} (already a dependency of \Otwo{})
    \item \HepMC{} (already a dependency of \Otwo{})
    \end{itemize}
  \end{itemize}
  \begin{alertblock}{New dependencies}
    \Rivet{} \& \Yoda{}
  \end{alertblock}
  \begin{itemize}
  \item Both plain C++ packages (C++11 and up) 
  \item Both use \emph{upstream} in \alidist{} (no \Alice{} specific
    patches)
  \item Both \emph{very} stable
  \end{itemize}
  
\end{frame}

%% -------------------------------------------------------------------
\begin{frame}
  \frametitle{In case compilation breaks\textellipsis}

  It's a big \emph{if} --- \Yoda{} and \Rivet{} are \emph{very}
  stable.
  
  \begin{enumerate}
  \item \alidist{} disable \Rivet{} dependency for \OtwoPhysics{} in
    deployment settings
  \item PWGMM/MC step in to fix issue together with \alidist{}
  \item \alidist{} re-enables \Rivet{} dependency for \OtwoPhysics{} in
    deployment settings
      
  \end{enumerate}
\end{frame}

%% -------------------------------------------------------------------
\begin{frame}
  \frametitle{Code elements}

  \begin{itemize}
  \item In \Otwo{}
    \begin{itemize}
    \item \texttt{AODToHepMC} in \texttt{libO2Generators}
    \item \texttt{o2-aod-producer-workflow} and
      \texttt{o2-aod-mc-producer-workflow} 
    \item \AOD{} tables \texttt{MCCollisions}, \texttt{MCParticles},
      \texttt{HepMCXSections}, \texttt{HepMCPdfInfos}, and
      \texttt{HepMCHeavyIons} 
    \end{itemize}
  \item In \OtwoRivet{}
    \begin{itemize}
    \item \texttt{o2-analysis-mm-rivet}
    \item \texttt{o2::rivet::RivetAOs} single data object stored in
      \texttt{AnalysisResults.root}. 
    \end{itemize}
  \end{itemize}
\end{frame}
%% ===================================================================
\begin{frame}
  \frametitle{Use cases}
  \begin{columns}[t,onlytextwidth]
    \begin{column}{.48\linewidth}
      \begin{block}<+->{Users}
        \emph{Anyone}\textellipsis
        \begin{itemize}
        \item comparing model to data for talks, manuscripts, etc.
          \begin{itemize}
          \item Ideally \emph{all} papers should have a \Rivet{}
            analysis 
          \end{itemize}
        \item ``\texttt{aliprod}'' as part of productions for model
          validation 
        \item doing proof-of-concept analysis 
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.48\linewidth}
      \begin{block}<+->{Frequency}
        Depends on user
        \begin{itemize}
        \item For talks, manuscripts, etc. --- \emph{once} per model
          compared to
        \item For ``\texttt{aliprod}'' --- when model is updated or
          parameters changed, new collision system or energy, etc. 
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

%% ===================================================================
\begin{frame}[fragile]
  \frametitle{\Rivet{} analyses}

  \begin{block}<+->{Anatomy}
    A \Rivet{} analysis (e.g.,
    \href{https://rivet.hepforge.org/analyses/ALICE_2012_I930312.html}{ALICE\_2012\_I930312})
    consists of
    \begin{itemize}
    \item A single source file,
    \item A \Yoda{} reference file, and
    \item optionally a \meta{meta}-data file
    \end{itemize}
  \end{block}

  \begin{block}<+->{Loading}
    \begin{itemize}
    \item One or more analysis compiled into shared library using
      \texttt{rivet-build}
\begin{verbatim}

    rivet-build RivetPWGMM.so PWGMM_foo.cxx PWGMM_bar.cxx

\end{verbatim}
    \item Loaded from \texttt{LD\_LIBRARY\_PATH} or specified path
    \item Reference \Yoda{} from specified path
    \end{itemize}
  \end{block}
\end{frame}

%% -------------------------------------------------------------------
\begin{frame}
  \frametitle{Types of \Rivet{} analyses}
  \begin{columns}[t,onlytextwidth]
    \begin{column}{.48\linewidth}
      \begin{block}<+->{Predefined}
        \begin{itemize}
        \item Those
          \href{https://rivet.hepforge.org/rivet-coverage}{available
            in \Rivet{}} 
        \item Many ``standard'' analyses --- e.g.,
          \texttt{MC\_FSPARTICLES} --- useful for model
          ``validation''. 
        \item Some 30 \Alice{} analyses (300+ outstanding)
        \item \emph{Always} available with \Rivet{} installation
        \end{itemize}
      \end{block}
    \end{column}
    \begin{column}{.48\linewidth}
      \begin{block}<+->{User supplied}
        \begin{itemize}
        \item Develop as part of regular analysis, manuscript
          preparation, or similar 
        \item Single C++ source, \Yoda{} reference, files
        \item Possibly ``centrality'' calibration, \meta{meta}-file
        \item Eventually submit to \Rivet{} upstream after validation
          (publication, \HepData{})
        \item Built into shared library (\texttt{rivet-build})
        \end{itemize}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

%% -------------------------------------------------------------------
\begin{frame}[fragile]
  \frametitle{Load \Rivet{} analyses in \Otwo{} deployment}

  \begin{itemize}[<+->]
  \item \emph{Predefined} trivially available 
  \item \emph{User} analyses require some thought
    \begin{itemize}[<.->]
    \item Own computer, CVMFS, Grid --- trivial --- compile on the fly
\begin{verbatim}

o2-analysis-mm-rivet --rivet-analyses PWGMM_foo,PWGMM_bar \
    --rivet-source PWGMM_foo.cxx,PWGMM_bar.cxx ...

\end{verbatim}
    \item Hyper-loop requires some thought 
    \end{itemize}
  \end{itemize}

  \begin{alertblock}<+->{Notice}
    How user \Rivet{} analyses are made available is (largely)
    \emph{independent} of \OtwoRivet{} deployment option.

    User analyses are \emph{transient} --- eventually they will be
    part of \Rivet{}.
  \end{alertblock}
\end{frame}

%% -------------------------------------------------------------------
\begin{frame}
  \frametitle{Hyper-loop analysis deployment options}

  Note, these requires a bit of investigation 
  \begin{itemize}
  \item As \emph{attachments} to wagons
    \begin{itemize}[<.->]
    \item Source, reference, and other files attached to wagon,
      uploaded and compiled as needed on worker 
    \end{itemize}
  \item Fetch from remote repository via \texttt{https} or similar
    \begin{itemize}[<.->]
    \item Source, reference, and other files downloaded and compiled
      as needed on worker
    \end{itemize}
  \item From \OtwoPhysics{} shared libraries built with \OtwoPhysics{}
    \begin{itemize}[<.->]
    \item Source, reference, and other files in
      \texttt{PWG}\meta{XX}\texttt{/Rivet}
    \item Would need a \textsc{Cmake} macro. 
    \end{itemize}
  \item Together with \OtwoRivet{} standalone package. 
    \begin{itemize}[<.->]
    \item Source, reference, and other files in
      \texttt{PWG}\meta{XX}\texttt{/Rivet}
    \item Package should be loadable in Hyper-loop.
    \end{itemize}
  \end{itemize}
\end{frame}
%% -------------------------------------------------------------------

\end{document}
%%
%% EOF
%%
