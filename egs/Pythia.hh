// -*- mode: C++ -*-
//
// Copyright 2023 Christian Holm Christensen
//
// This software is distributed under the terms of the GNU General
// Public License v3 (GPL Version 3), copied verbatim in the file
// "COPYING".
//
// In applying this license CERN does not waive the privileges and
// immunities granted to it by virtue of its status as an
// Intergovernmental Organization or submit itself to any
// jurisdiction.
//
#ifndef MyPythia_h
#define MyPythia_h
#include <string>
#include <iterator>
#include <HepMC3/WriterAscii.h>
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC3.h"
#include <cassert>
#include <numeric>
#include <random>
#include <chrono>
#define LOG(X) std::cerr

// ===================================================================
struct BasePythia
{
  /** Program name */
  std::string _progname;
  /** Configuration file to read */
  std::string _config = "";
  /** Output file name */
  std::string _output = "out.hepmc";
  /** Number of events to make */
  size_t _nev = 10;
  /** Maximum number of errors */
  size_t _err = 10;
  /** Current number of errors */
  size_t _nerr = 0;
  /** Precision to use */
  int _prec   = 16;
  /** Maximum impact parameter */
  float _bMax = 14.5;
  /** Random number seed. Negative use default, 0 use time, other used as is */
  long _seed;
  /** Wether to prune event */
  bool _prunePythia = false;
  /** Wether to prune event */
  bool _pruneHepMC = false;
  /** Wether to prune event */
  bool _pruneOld = false;
  /** Be verbose */
  bool _verb = false;
  /** Pythia, arguments are data directory and whether to print banner */
  Pythia8::Pythia _pythia{"",false};
  
  virtual void usage(std::ostream& o=std::cout)
  {
    o << "Usage: " << _progname << " [OPTIONS]\n\n"
      << "Options:\n"
      << "  -h             Show this help\n"
      << "  -n NEV         Generate NEV events (" << _nev << ")\n"
      << "  -e MAX         At most MAX faulty events (" << _err << ")\n"
      << "  -c CONFIG      Read configuration from CONFIG (" << _config << ")\n"
      << "  -o OUTPUT      Write output to OUTPUT (" << _output << ")\n"
      << "  -p PRECISION   Floating point precision used (" << _prec << " - 2 to 24)\n"
      << "  -b FERMI       Maximum impact parameter (" << _bMax << " fm)\n"
      << "  -P             Prune Pythia event (" << (_prunePythia ? "yes" : "no")  << "\n"
      << "  -Q             Prune HepMC event (" << (_pruneHepMC ? "yes" : "no")
      << "\n"
      << "  -O             Prune (old) event (" << (_pruneOld ? "yes" : "no")
      << "\n"
      << "  -v             Be verbose\n"
      << std::endl;
  }

  enum class OptionStatus {
    OK,
    OK_EAT,
    EXIT,
    BAD
  };
  
  virtual OptionStatus parseOption(char option, char* arg)
  {
    switch (option) {
    case 'n': _nev         = std::stoi(arg); return OptionStatus::OK_EAT;
    case 'e': _err         = std::stoi(arg); return OptionStatus::OK_EAT;
    case 'c': _config      = arg;            return OptionStatus::OK_EAT;
    case 'o': _output      = arg;            return OptionStatus::OK_EAT;
    case 'p': _prec        = std::stoi(arg); return OptionStatus::OK_EAT;
    case 's': _seed        = std::stol(arg); return OptionStatus::OK_EAT;
    case 'b': _bMax        = std::stof(arg); return OptionStatus::OK_EAT;
    case 'P': _prunePythia = true;           return OptionStatus::OK;
    case 'Q': _pruneHepMC  = true;           return OptionStatus::OK;
    case 'O': _pruneOld    = true;           return OptionStatus::OK;
    case 'v': _verb        = true;           return OptionStatus::OK;
    }
    return OptionStatus::BAD;
  }

  virtual void unknownOption(char option)
  {
    throw std::runtime_error(std::string("Unknown option")+" -"+
			     std::string(1,option));
  }

  virtual void silence()
  {
    _pythia.readString("Print:quiet                      = on");
    _pythia.readString("Init:showAllSettings             = off");
    _pythia.readString("Stat:showProcessLevel            = off");
    _pythia.readString("Init:showAllParticleData         = off");
    _pythia.readString("Init:showChangedParticleData     = off");
    _pythia.readString("Init:showChangedSettings         = off");
    _pythia.readString("Init:showMultipartonInteractions = off");
  }

  virtual void seed()
  {
    _pythia.readString("Random:setSeed = "
		       +std::string(_seed >= 0 ? "yes" : "no"));
    _pythia.readString("Random:seed = "+std::to_string(_seed));
  }
  
  virtual void configure() = 0;

  virtual void init()
  {
    std::cerr << "Initalising ." << std::flush;
    // Suppress output 
    silence();

    // Set the random number seed
    std::cerr << "." << std::flush;
    seed();
    
    // Configure the EG
    std::cerr << "." << std::flush;
    configure();
    
    // Read in commands from external file.
    std::cerr << "." << std::flush;
    if (not _config.empty()) _pythia.readFile(_config);

    // Initialization.
    std::cerr << "." << std::flush;
    _pythia.init();
    std::cerr << " done" << std::endl;
  }

  virtual bool makeEvent()
  {
    if (not _pythia.next()) {
      // First few failures write off as "acceptable" errors, then quit.
      if (++_nerr < _err) return true;
      std::cerr << " Event generation failed " << _nerr << " times"
		<< std::endl;
      return false;
    }
    return true;
  }

  /** Prune event of particles that are not real.

      Implementation currently in O2 GeneratorPythia8.  This is
      broken! */
  virtual void pruneOld()
  {
    // Make a pruned event 
    Pythia8::Event pruned;
    pruned.init("Pruned event", &_pythia.particleData);
    pruned.reset();

    // Get the original event 
    Pythia8::Event& event = _pythia.event;
    
    std::unordered_map<size_t,size_t> old2new;
    std::list<Pythia8::Particle> filtered;

    // Push system particle(?)
    filtered.push_back(event[0]);

    // Loop over particles and store those we need 
    for (size_t i = 0; i < event.size(); i++) {
      auto& particle = event[i];

      if (particle.statusHepMC() == 1 or
	  particle.statusHepMC() == 2 or
	  particle.statusHepMC() == 1) {
	filtered.push_back(particle);
	old2new[i] = filtered.size()-1;
      }
    }

    // Utility to find new index, or -1 if not found
    auto findNew = [old2new](size_t old) -> int {
      auto iter = old2new.find(old);
      return iter == old2new.end() ? -1 : iter->second;
      // return old2new[old];
    };

    for (auto& particle : filtered) {
      auto  daughters = particle.daughterList();
      std::vector<size_t> newDaughters;
      
      for (auto daughterNo : daughters) {
	int daughterNew = findNew(daughterNo);
	if (daughterNew < 0) continue;

	newDaughters.push_back(daughterNew);
      }

      auto firstDaughter  = particle.daughter1();
      auto lastDaughter = particle.daughter2();

      if (firstDaughter == 0 and lastDaughter == 0)
	assert(newDaughters.size() == 0);
      else if (firstDaughter == lastDaughter and firstDaughter != 0) {
	assert(newDaughters.size() == 1);
	particle.daughters(newDaughters[0],newDaughters[0]);
      }
      else if (firstDaughter > 0 and lastDaughter == 0) {
	assert(newDaughters.size() == 1);
	particle.daughters(newDaughters[0],0);
      }
      else if (lastDaughter != 0 and lastDaughter > firstDaughter) {
	assert(newDaughters.size() == lastDaughter-firstDaughter+1);
	particle.daughters(findNew(firstDaughter),
			   findNew(lastDaughter));
      }
      else if (lastDaughter != 0 and lastDaughter < firstDaughter) {
	assert(newDaughters.size() == 2);
	particle.daughters(findNew(firstDaughter),
			   findNew(lastDaughter));
      }

      auto firstMother = particle.mother1();
      auto lastMother  = particle.mother2();

      if (firstMother > 0) {
	int newFirst = findNew(firstMother);
	if (lastMother == firstMother or lastMother == 0) {
	  if (newFirst < 0) particle.mothers(0,0); // Mother is gone
	  else              particle.mothers(newFirst,newFirst);
	}
	if (firstMother < lastMother) {
	  // Remove mothers
	  particle.mothers(0,0);
	  // See if we can find them again
	  int newLast = findNew(lastMother);

	  if (newFirst > 0 or newLast > 0) {
	    // Weird!
	  }
	}
      }
      pruned.append(particle);
    }

    // Assign pruned event
    _pythia.event = pruned;
  }

  /** Mapping from old to new index */
  // using Old2New = std::map<size_t,size_t>;
  using Old2New = std::vector<int>;
  /** Container of flags whether a particle has been done or not */
  using Done    = std::vector<bool>;
  /** Function type to get some relatives, e.g., mothers */  
  typedef std::vector<int> (*Getter)(const Pythia8::Particle&);
  /** Function type to set relatvies, e.g., daughters */
  typedef void (*Setter)(Pythia8::Particle&,int,int);
  /** Function type to set relatvies, e.g., daughters */
  typedef std::pair<int,int> (*FirstLast)(Pythia8::Particle&);

  /** Investigate the relatives of a particle, and get the (possibly)
      new set of relatives where unneeded particles have been pruned.
      This calls itself recursively. */ 
  virtual void investigateRelatives(Pythia8::Event&       oldEvent,
				    const Old2New&        old2New,
				    size_t                oldIndex,
				    Done&                 done,
				    Getter                getter,
				    Setter                setter,
				    FirstLast             firstLast,
				    const std::string&    what,
				    const std::string&    ind="")
  {
    // Utility to find new index, or -1 if not found
    auto findNew = [old2New](size_t old) -> int {
      // auto iter = old2New.find(old);
      // return iter == old2New.end() ? -1 : iter->second;
      return old2New[old];
    };
    int newIdx = findNew(oldIndex);
    int hepmc  = oldEvent[oldIndex].statusHepMC();

    if (_verb)
      std::cerr << ind
		<< oldIndex << " -> "
		<<  newIdx << " (" << hepmc << ") ";
    if (done[oldIndex]) {
      if (_verb) std::cerr << " already done" << std::endl;
      return;
    }


    // Our list of new relatives
    // using IdList=std::set<int>;
    // IdList newRelatives;
    using IdList = std::pair<int,int>;
    constexpr int invalid = 0xFFFFFFF;
    IdList newRelatives = std::make_pair(invalid,-invalid);
    
    // Utility to add id
    // auto addId = [](IdList& l, size_t id) { l.insert(id); };
    auto addId = [](IdList& l, size_t id) {
      l.first  = std::min(int(id), l.first);
      l.second = std::max(int(id), l.second);
    };
			 
    auto relatives = getter(oldEvent[oldIndex]);

    if (_verb) {
      auto fl = firstLast(oldEvent[oldIndex]);
      std::cerr << "check " << what << "s ["
		<< std::setw(3) << fl.first << ","
		<< std::setw(3) << fl.second << "] "
		<< relatives.size() << std::endl;
    }
    
    
    for (auto relativeIdx : relatives) {
      int newRelative = findNew(relativeIdx);
      if (newRelative >= 0) {
	// If this relative is to be kept, then append to list of new
	// relatives.
	if (_verb) 
	  std::cerr << ind << " "
		    << what << " "
		    << relativeIdx << " -> "
		    << newRelative << " to be kept" << std::endl;
	addId(newRelatives,newRelative);
	continue;
      }
      if (_verb) 
	std::cerr << ind << " "
		  << what << " "
		  << relativeIdx << " not to be kept "
		  << (done[relativeIdx] ? "already done" : "to be done")
		  << std::endl;

      // Below is code for when the relative is not to be kept 
      auto& relative = oldEvent[relativeIdx];
      if (not done[relativeIdx]) {
	// IF the relative hasn't been processed yet, do so now
	investigateRelatives(oldEvent,
			     old2New,
			     relativeIdx,
			     done,
			     getter,
			     setter,
			     firstLast,
			     what,
			     ind+"  ");
      }
      
      // If this relative was already done, then get its relatives and
      // add them to the list of new relatives.
      if (_verb)
	std::cerr << ind << " "
		  << what << " "
		  << relativeIdx << " gave new relatives ";
      // for (auto grandRelative : getter(relative)) {
      // 	if (_verb) std::cerr << grandRelative << ", ";
      // 	addId(newRelatives,grandRelative);
      // }
      // auto grandRelatives = getter(relative);
      // auto grandRelative1 = *std::min_element(grandRelatives.begin(),
      // 				      grandRelatives.end());
      // auto grandRelative2 = *std::max_element(grandRelatives.begin(),
      //   				      grandRelatives.end());
      auto grandRelatives = firstLast(relative);
      int grandRelative1 = grandRelatives.first;
      int grandRelative2 = grandRelatives.second;
      assert (grandRelative1 != invalid);
      assert (grandRelative2 != -invalid);
      if (grandRelative1 > 0) addId(newRelatives,grandRelative1);
      if (grandRelative2 > 0) addId(newRelatives,grandRelative2);
      if (_verb)
	std::cerr << grandRelative1 << " -> " << grandRelative2 << std::endl;
    }
    auto& particle = oldEvent[oldIndex];
    if (_verb) {
      std::cerr << ind << " Got "
		<< (newRelatives.second-newRelatives.first+1) << " new "
	//<< newRelatives.size()	<< " new "
		<<  what << "s ";
      // for (auto relativeIdx : newRelatives)
      //   std::cerr << relativeIdx << ",";
      std::cerr << std::endl;
    }
  

    if (newRelatives.first != invalid) {
      int newRelative1 = newRelatives.first;
      int newRelative2 = newRelatives.second;
      setter(particle,newRelative1,newRelative2);
      if (_verb) {
	auto fl = firstLast(particle);
	std::cerr << ind << " "
		  << what << "s: "
		  << fl.first << " (" << newRelative1 << "),"
		  << fl.second << " (" << newRelative2 << ")" << std::endl;
      }
    }
    else
      setter(particle,0,0);
    done[oldIndex] = true;
  }

  typedef bool (*PythiaSelect)(const Pythia8::Particle&);
  
  virtual void pruneEvent(PythiaSelect select)
  {
    // Get the original event 
    Pythia8::Event& event = _pythia.event;

    // Mapping from old to new index. 
    Old2New old2new(event.size(),-1);

    // Particle 0 is a system particle, and we will skip that in the
    // following.
    // filtered.push_back(event[0]);
    size_t newId = 0;

    // Loop over particles and store those we need 
    for (size_t i = 1; i < event.size(); i++) {
      auto& particle = event[i];
      if (select(particle)) {
	++newId;
	// filtered.push_back(particle);
	old2new[i] = newId;
	// filtered.size()-1;
      }
    }
    // Utility to find new index, or -1 if not found
    auto findNew = [old2new](size_t old) -> int {
      // auto iter = old2new.find(old);
      // return iter == old2new.end() ? -1 : iter->second;
      return old2new[old];
    };

    if (_verb) {
      for (size_t i = 1; i < event.size(); i++) {
	auto  newIdx   = findNew(i);
	auto& particle = event[i];
	std::cerr << std::setw(3) << i << " -> "
		  << std::setw(3) << newIdx << " "
		  << std::setw(3) << particle.statusHepMC() << "/"
		  << std::setw(5) << particle.status() << " "
		  << std::setw(6) << particle.id() << " "
		  << std::setw(3) << particle.mother1() << ","
		  << std::setw(3) << particle.mother2() << "] ["
		  << std::setw(3) << particle.daughter1() << ","
		  << std::setw(3) << particle.daughter2() << "] "
		  << std::endl;
      }
    }

    // First loop, investigate mothers - from the bottom
    auto getMothers = [](const Pythia8::Particle& particle) {
      return particle.motherList(); };
    auto setMothers = [](Pythia8::Particle& particle, int m1, int m2) {
      particle.mothers(m1,m2); };
    auto firstLastMothers = [](Pythia8::Particle& particle)
      -> std::pair<int,int>  {
      return std::make_pair(particle.mother1(),particle.mother2()); };
    auto getDaughters = [](const Pythia8::Particle& particle) {
      // In case of |status|==13 (diffractive), we cannot use
      // Pythia8::Particle::daughterList as it will give more than
      // just the immediate daughters. In that cae, we do it
      // ourselves.
      if (std::abs(particle.status()) == 13) {
	int d1 = particle.daughter1();
	int d2 = particle.daughter2();
	if (d1 == 0 and d2 == 0) return std::vector<int>();
	if (d2 == 0) return std::vector<int>{d1};
	if (d2 > d1) {
	  std::vector<int> ret(d2-d1+1);
	  std::iota(ret.begin(), ret.end(), d1);
	  return ret;
	}
	return std::vector<int>{d2,d1};
      }
      return particle.daughterList(); };
    auto setDaughters = [](Pythia8::Particle& particle, int d1, int d2) {
      particle.daughters(d1,d2); };
    auto firstLastDaughters = [](Pythia8::Particle& particle)
      -> std::pair<int,int> {
      return std::make_pair(particle.daughter1(),particle.daughter2()); };

    Done motherDone(event.size(), false);
    for (size_t i = 1; i < event.size(); ++i) 
      investigateRelatives(event, old2new, i, motherDone,
			   getMothers, setMothers,
			   firstLastMothers, "mother");

    // Second loop, investigate daughters - from the top
    Done daughterDone(event.size(), false);
    for (size_t i = event.size()-1; i > 0; --i) 
      investigateRelatives(event, old2new, i, daughterDone,
			   getDaughters, setDaughters,
			   firstLastDaughters, "daughter");

    // Make a pruned event 
    Pythia8::Event pruned;
    pruned.init("Pruned event", &_pythia.particleData);
    pruned.reset();
    // Add system 
    // pruned.append(event[0]);
    
    for (size_t i = 1; i < event.size(); i++) {
      int newIdx = findNew(i);
      if (newIdx < 0) continue;
      
      auto particle = event[i];
      int realIdx = pruned.append(particle);
      assert(realIdx == newIdx);
    }

    // We may have that two or more mothers share some daughters, but
    // that one or more mothers have more daughters than the other
    // mothers, and hence not all daughters point back to all mothers.
    // This can happen, for example, if a beam particle radiates
    // on-shell particles before an interaction with any daughters
    // from the other mothers.  Thus, we need to take care of that or
    // the event record will be corrupted.
    //
    // What we do is that for all particles, we look up the daughters.
    // Then for each daughter, we check the mothers of those
    // daughters.  If this list of mothers include other mothers than
    // the currently investigated mother, we must change the mothers
    // of the currently investigated daughters.
    using IdList=std::pair<int,int>;
    // Utility to add id
    auto addId = [](IdList& l, size_t id) {
      l.first  = std::min(int(id), l.first);
      l.second = std::max(int(id), l.second);
    };
    constexpr int invalid = 0xFFFFFFF;
    
#if 1
    Done shareDone(pruned.size(), false);
    for (size_t i = 1; i < pruned.size(); i++) {
      if (shareDone[i]) continue;
      
      auto& particle  = pruned[i];
      auto  daughters = particle.daughterList();
      IdList allDaughters = std::make_pair(invalid,-invalid);
      IdList allMothers   = std::make_pair(invalid,-invalid);
      addId(allMothers, i);
      for (auto daughterIdx : daughters) {
	// Add this daughter to set of all daughters
	addId(allDaughters,daughterIdx);
	auto& daughter     = pruned[daughterIdx];
	auto  otherMothers = daughter.motherList();
	for (auto otherMotherIdx : otherMothers) {
	  // Add this mother to set of all mothers.  That is, take all
	  // mothers of the current daughter of the current particle
	  // and store that.  In this way, we register mothers that
	  // share a daughter with the current particle.
	  addId(allMothers,otherMotherIdx);
	  // We also need to take all the daughters of this shared
	  // mother and reister those.
	  auto& otherMother    = pruned[otherMotherIdx];
	  // auto  otherDaughters = otherMother.daughterList();
	  // if (otherDaughters.size() > 0) {
	  //   int   otherDaughter1 = *std::min_element(otherDaughters.begin(),
	  // 					     otherDaughters.end());
	  //   int   otherDaughter2 = *std::max_element(otherDaughters.begin(),
	  // 					     otherDaughters.end());
	  //   addId(allDaughters,otherDaughter1);
	  //   addId(allDaughters,otherDaughter2);
	  // }
	  int otherDaughter1 = otherMother.daughter1();
	  int otherDaughter2 = otherMother.daughter2();
	  if (otherDaughter1 > 0) addId(allDaughters,otherDaughter1);
	  if (otherDaughter2 > 0) addId(allDaughters,otherDaughter2);
	}
	// At this point, we have added all mothers of current
	// daughter, and all daughters of those mothers.
      }
      // At this point, we have all mothers that share daughters with
      // the current particle, and we have all of the daughters
      // too.
      // 
      // We can now update the daughter information on all mothers
      int minDaughter = allDaughters.first;
      int maxDaughter = allDaughters.second;
      int minMother   = allMothers.first;
      int maxMother   = allMothers.second;
      if (_verb) {
	std::cerr << std::setw(3) << minDaughter << " -> "
		  << std::setw(3) << maxDaughter << " and "
	          << std::setw(3) << minMother << " -> "
		  << std::setw(3) << maxMother << std::endl;
      }
      if (minMother != invalid) {
	// If least mother isn't invalid, then largest mother will not
	// be invalid either.
	for (size_t motherIdx = minMother; motherIdx <= maxMother; //
	     motherIdx++) {
	  shareDone[motherIdx] = true;
	  if (minDaughter == invalid)
	    pruned[motherIdx].daughters(0,0);
	  else
	    pruned[motherIdx].daughters(minDaughter,maxDaughter);
	}
      }
      if (minDaughter != invalid) {
	// If least mother isn't invalid, then largest mother will not
	// be invalid either.
	for (size_t daughterIdx = minDaughter; daughterIdx <= maxDaughter; //
	     daughterIdx++) 
	  if (minMother   == invalid)
	    pruned[daughterIdx].mothers(0,0);
	  else 
	    pruned[daughterIdx].mothers(minMother,maxMother);
      }
    }
#endif
    
    if (_verb) {
      // Cannot do this printing until after _all_ particles have been
      // added to the pruned event.  Some of the member functions
      // actually reference back to the event and looks up particles
      // from that, which means all particles must be in.
      for (size_t i = 1; i < event.size(); i++) {
	int newIdx = findNew(i);
	if (newIdx < 0) continue;
	auto& newParticle = pruned[newIdx];
	std::cerr << std::setw(3) << i << " -> "
		  << std::setw(3) << newIdx << " "
		  << std::setw(3) << newParticle.statusHepMC() << "/"
		  << std::setw(5) << newParticle.status() << " "
		  << std::setw(6) << newParticle.id() << " "
		  << std::setw(3) << newParticle.mother1() << ","
		  << std::setw(3) << newParticle.mother2() << "] ["
		  << std::setw(3) << newParticle.daughter1() << ","
		  << std::setw(3) << newParticle.daughter2() << "] "
		  << std::endl;
      }
    }
      
    _pythia.event = pruned;    
  }
  typedef bool (*HepMCSelect)(HepMC3::ConstGenParticlePtr);
  virtual void pruneEvent(HepMC3::GenEvent& event,
			  HepMCSelect       select)
  {
    auto particles = event.particles();
    auto vertices  = event.vertices();
    std::list<HepMC3::GenParticlePtr> toRemove;

    if (_verb) {
      std::cerr << "HepMC events has " << particles.size()
		<< " particles and " << vertices.size()
		<< " vertices" << std::endl;
    }
    size_t nSelect = 0;

    // Below is for testing random entry
    // 
    // std::vector<size_t> indexes(particles.size());
    // std::iota(indexes.begin(), indexes.end(), 0);
    // if (false /*randomOrder*/) {
    //    std::random_device rd;
    //    std::mt19937 g(rd());
    // 
    //    std::shuffle(indexes.begin(), indexes.end(), g);
    // }
    // for (size_t i : indexes)
    for (size_t i = 0; i < particles.size(); i++) {
      auto particle = particles[i];
      if (select(particle)) {
	nSelect++;
	continue;
      }

      // Remove particle from the event
      toRemove.push_back(particle);
      if (_verb)
	std::cerr << "Remove "
		  << std::setw(3) << particle->id() << " " << std::flush;
      
      auto endVtx   = particle->end_vertex();
      auto prdVtx   = particle->production_vertex();
      if (endVtx) {
	// Disconnect this particle from its out going vertex
	endVtx->remove_particle_in(particle);
	if (_verb) 
	  std::cerr << "end " << std::setw(3) << endVtx->id()
		    << " " << std::flush;

	if (prdVtx and prdVtx->id() != endVtx->id()) {
	  if (_verb) 
	    std::cerr << "prd " << std::setw(3) << prdVtx->id() << " "
		      << std::flush;
	  
	  auto outbound = endVtx->particles_out();
	  if (_verb) std::cerr << std::setw(3) << outbound.size() << " out ";
	  for (auto outgoing : outbound) {
	    // This should also detach the particle from its old
	    // end-vertex.
	    if (outgoing) {
	      auto ee = outgoing->end_vertex();
	      if (not ee or ee->id() != prdVtx->id())
		prdVtx->add_particle_out(outgoing);
	      if (_verb)
		std::cerr << std::setw(3) << outgoing->id() << " ";
	    }
	  }

	  auto inbound = endVtx->particles_in();
	  if (_verb) std::cerr << std::setw(3) << inbound.size() << " in ";
	  for (auto incoming : inbound) {
	    if (incoming) {
	      auto pp = incoming->production_vertex();
	      if (not pp or pp->id() != prdVtx->id())
		prdVtx->add_particle_in(incoming);
	      if (_verb)
		std::cerr << std::setw(3) << incoming->id() << " ";
	    }
	  }
	}
      }
      if (prdVtx) {
	prdVtx->remove_particle_out(particle);
      }
      if (_verb)
	std::cerr << std::endl;
    }

    if (_verb) 
      std::cerr << "Selected " << nSelect << " particles\n"
		<< "Removing " << toRemove.size() << " particles" << std::endl;
    // This is unfortunately highly in efficient.  This does a lot of
    // deallocation of std::vector objects which makes it really slow.
    for (auto particle : toRemove) event.remove_particle(particle);
    
    
    std::list<HepMC3::GenVertexPtr> remVtx;
    for (auto vtx : event.vertices()) {
      if (not vtx or
	  (vtx->particles_out().empty() and
	   vtx->particles_in ().empty())) remVtx.push_back(vtx);
    }
    if (_verb) std::cerr << "Removing " << remVtx.size() << " vertexes"
			<< std::endl;
    // This is unfortunately highly in efficient.  This does a lot of
    // deallocation of std::vector objects which makes it really slow.
    for (auto vtx: remVtx) event.remove_vertex(vtx);
    
    if (_verb) 
      std::cerr << "HepMC events has " << event.particles().size()
		<< " particles and " << event.vertices().size()
		<< " vertices" << std::endl;
    
  }
  
  virtual int loop()
  {
    // Open output file (or use std::cout)
    std::ofstream* fout = nullptr;
    if (_output != "-") 
      fout = new std::ofstream(_output.c_str());
    std::ostream& out = fout ? *fout : std::cout;
    std::cerr << "Write output to " << _output << std::endl;
    
    // HepMC interface
    HepMC3::GenEvent    event;
    HepMC3::WriterAscii writer(out);
    
    // Interface for conversion from Pythia8::Event to HepMC event.
    // Specify file where HepMC events will be stored.
    HepMC3::Pythia8ToHepMC3 converter;

    // Select pythia particles
    auto pythiaSelect = [](const Pythia8::Particle& particle) {
      switch(particle.statusHepMC()) {
      case 1: // Final st
      case 2: // Decayed 
      case 4: // Beam    
	return true;
      }
      // if (particle.id() == 9902210) return true;
      return false;
    };
    // Select HepMC particles
    auto hepMCSelect = [](HepMC3::ConstGenParticlePtr particle) {
      switch (particle->status()) {
      case 1: // Final st
      case 2: // Decayed 
      case 4: // Beam    
	return true;
      }
      // if (particle->pid() == 9902210) return true;
      return false;
    };
    // Begin event loop.
    size_t _nerr = 0;
    for (size_t iev = 0; iev < _nev; ++iev) {
      std::cerr << "Event # " << std::setw(6) << iev << std::flush;
      // Generate event.     
      if (not makeEvent()) return 1;

      size_t nOrig = _pythia.event.size();
      std::cerr << " " << std::setw(12) << nOrig;
      auto   t1    = std::chrono::high_resolution_clock::now();
      if (_prunePythia) pruneEvent(pythiaSelect);
      if (_pruneOld)    pruneOld();
      auto   t2    = std::chrono::high_resolution_clock::now();
      size_t nPrune = _pythia.event.size();

      
      // Construct new empty HepMC event, fill it and write it out.
      event.clear();
      converter.fill_next_event(_pythia, event);
      if (_pruneHepMC) {
	t1    = std::chrono::high_resolution_clock::now();
	pruneEvent(event,hepMCSelect);
	t2    = std::chrono::high_resolution_clock::now();
	nPrune = event.particles().size();
      }
      auto dur = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1);
      
      std::cerr << " " << std::setw(12) << nPrune << " " << std::setw(18)
		<< dur.count() << std::endl;
      
      writer.write_event(event);
      // Flush output 
      out << std::flush;
    }
    return 0;
  }

  int run(int argc, char** argv)
  {
    _progname = argv[0];
    for (int i = 1; i < argc; i++) {
      if (argv[i][0] == '-') {
	if (argv[i][1] == 'h') { usage(); return 0; }
	auto status = parseOption(argv[i][1], argv[i+1]);
	switch (status) {
	case OptionStatus::OK_EAT:  ++i; // Fall through
	case OptionStatus::OK:      break;
	case OptionStatus::EXIT:    return 0;
	case OptionStatus::BAD: unknownOption(argv[i][1]);
	}
      }
      else
	throw std::runtime_error(std::string("Non-option: ")+argv[i]);
    }
   
    init();
    std::cerr << "Initialisation done, starting loop" << std::endl;
    
    return loop();
  }
};

      

// ===================================================================
template <typename MyPythia>
int doMain(int argc, char** argv)
{
  //try {
    MyPythia myPythia;
    return myPythia.run(argc,argv);
    //}
    //catch (std::exception& e) {
    // std::cerr << argv[0] << ": " << e.what() << std::endl;
    // }
  return 1;
}
#endif
//
// EOF
//
