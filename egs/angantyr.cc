// -*- mode: C++ -*-
//
// Copyright 2023 Christian Holm Christensen
//
// This software is distributed under the terms of the GNU General
// Public License v3 (GPL Version 3), copied verbatim in the file
// "COPYING".
//
// In applying this license CERN does not waive the privileges and
// immunities granted to it by virtue of its status as an
// Intergovernmental Organization or submit itself to any
// jurisdiction.
//
/*
  Compile with

    g++ -std=c++17 -I/opt/sw/inst/include angantyr.cc \
    	-o angatyr \
    	-L/opt/sw/inst/lib
    	-Wl,-rpath,/opt/sw/inst/lib
    	-lHepMC3
    	-lpythia8
*/
#include "Pythia.hh"

// ===================================================================
struct PbPbPythia : BasePythia
{
  /** Fit cross-section */
  bool         _fitSig = false;
  std::string  _model = "random";
  const std::set<std::string> models = {"fixed","random","opacity"};
  
  PbPbPythia() : BasePythia() { _output = "angantyr.hepmc"; }
  
  virtual void usage(std::ostream& o=std::cout)
  {
    BasePythia::usage();
    o << "  -f             Fit cross-section (" << (_fitSig ? "yes" : "no") << ")\n"
      << "  -m MODEL       Angantyr model (" << _model
      << " - one of fixed,random,opacity)" << std::endl;
  }
  
  virtual OptionStatus parseOption(char option, char* arg)
  {
    switch (option) {
    case 'f': _fitSig = !_fitSig; return OptionStatus::OK;
    case 'm': _model  = arg;      return OptionStatus::OK_EAT;
    }
    return BasePythia::parseOption(option, arg);
  }

  virtual void configure()
  {
    size_t modNo  = std::distance(models.begin(),  models.find(_model));
    if (modNo > models.size())
      throw std::runtime_error(std::string("Unknown Angantyr model ")+_model);
    
    const std::vector<double> sigFit = {13.91,1.78,0.22,0.0,0.0,0.0,0.0,0.0};
    std::stringstream s;
    std::copy(sigFit.begin(), sigFit.end(),
	      std::ostream_iterator<double>(s, ","));
    std::string sigPar = s.str();
    sigPar             = sigPar.substr(0,sigPar.size()-1);
    
    // Angantyr model parameters 
    _pythia.readString("Angantyr::CollisionModel     = "+std::to_string(modNo));
    _pythia.readString("HeavyIon:showInit            = off");
    _pythia.readString("HeavyIon:SigFitPrint         = off");
    _pythia.readString("HeavyIon:SigFitDefPar        = "+sigPar);
    _pythia.readString("HeavyIon:SigFitNGen          = "+std::string(_fitSig?"20":"0"));
    _pythia.readString("HeavyIon:bWidth              = "+std::to_string(_bMax));

    // Beam parameter settings. Values below agree with default ones.
    _pythia.readString("Beams:idA                    = 1000822080");
    _pythia.readString("Beams:idB                    = 1000822080");
    _pythia.readString("Beams:eCM                    = 5020.");//As above
    _pythia.readString("Beams:frameType              = 1");
    _pythia.readString("ParticleDecays:limitTau0     = on");
    _pythia.readString("ParticleDecays:tau0Max       = 10");
  }
};


int main(int argc, char** argv)
{
  return doMain<PbPbPythia>(argc, argv);
}
//
// EOF
//
