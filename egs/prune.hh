#ifndef PRUNE_HH
#define PRUNE_HH
#include <HepMC3/GenEvent.h>
#include <HepMC3/GenParticle.h>
#include <HepMC3/GenVertex.h>
#include <HepMC3/WriterAscii.h>
#include <HepMC3/Setup.h>
#include <iostream>
#include <fstream>
#include <list>

#define LOG(ENABLE) \
  if (ENABLE)       \
  std::clog

template <typename Select>
void prune(HepMC3::GenEvent& event,
           Select select,
           int verb = 0)
{
  auto   particles = event.particles();
  auto   vertices  = event.vertices();
  size_t nPart     = particles.size();
  size_t nVtx      = vertices.size();
  std::list<HepMC3::GenParticlePtr> toRemove;

  LOG(verb > 1) << "HepMC events has " << nPart
                << " particles and " << nVtx
                << " vertices" << std::endl;

  size_t nSelect = 0;

  for (auto particle : particles) {
    if (select(particle)) {
      nSelect++;
      continue;
    }

    // Remove particle from the event
    toRemove.push_back(particle);
    LOG(verb > 2) << "Remove "
                  << std::setw(3) << particle->id() << " " << std::flush;

    auto endVtx = particle->end_vertex();
    auto prdVtx = particle->production_vertex();
    if (endVtx) {
      LOG(verb > 3) << "end " << std::setw(3) << endVtx->id()
                    << " " << std::flush;

      // If we have a production-vertex, and it is not the same as the
      // end-vertex, then we need to move particles.
      if (prdVtx and prdVtx->id() != endVtx->id()) {
        LOG(verb > 3) << "prd " << std::setw(3) << prdVtx->id() << " "
                      << std::flush;

        auto outbound = endVtx->particles_out();
        LOG(verb > 3) << std::setw(3) << std::distance(outbound.begin(), outbound.end()) << " out ";

        // All particles that are leaving the current particles end
        // vertex must be moved to exit the current particles
        // production vertex.
        for (auto outgoing : outbound) {
          if (not outgoing)
            continue;

          auto ee = outgoing->end_vertex();
          // If the outgoing particle has no end vertex or its end
          // vertex isn't the same as the production vertex of the
          // current particle, then add it to the production vertex of
          // the current particle.
          if (not ee or ee->id() != prdVtx->id())
            prdVtx->add_particle_out(outgoing);
          LOG(verb > 4) << std::setw(3) << outgoing->id() << " ";
        }

        auto inbound = endVtx->particles_in();
        LOG(verb > 3) << std::setw(3)
                      << std::distance(inbound.begin(), inbound.end()) << " in ";

        // All particles that are entering the current particles end
        // vertex must be moved to enter the current particles
        // production vertex.
        for (auto incoming : inbound) {
          if (not incoming or incoming == particle)
            continue;

          auto pp = incoming->production_vertex();
          if (not pp or pp->id() != prdVtx->id())
            prdVtx->add_particle_in(incoming);

          LOG(verb > 4) << std::setw(3) << incoming->id() << " ";
        }
      }
    }
    if (endVtx) {
      // Disconnect this particle from its end vertex
      endVtx->remove_particle_in(particle);
    }
    if (prdVtx) {
      // Disconnect this particle from its production vertex
      prdVtx->remove_particle_out(particle);
    }
    LOG(verb > 2) << std::endl;
  }

  LOG(verb > 1) << "Selected " << nSelect << " particles\n"
                << "Removing " << toRemove.size() << " particles" << std::endl;

  // This is unfortunately highly in efficient.  This does a lot of
  // deallocation of std::vector objects which makes it really slow.
  for (auto particle : toRemove)
    event.remove_particle(particle);

  std::list<HepMC3::GenVertexPtr> remVtx;
  for (auto vtx : event.vertices()) {
    if (vtx and
        vtx->id() < 0 and
        (vtx->particles_out().empty() and
         vtx->particles_in().empty()))
      remVtx.push_back(vtx);
  }

  LOG(verb > 1) << "Removing " << remVtx.size() << " vertexes"
                << std::endl;
  // This is unfortunately highly in efficient.  This does a lot of
  // deallocation of std::vector objects which makes it really slow.
#ifndef PRUNE_BUG
  for (auto vtx : remVtx)
    event.remove_vertex(vtx);
#endif

  LOG(verb > 0) << "HepMC event originally had "
                << nPart << " particles and " << nVtx << " vertices, "
                << "has been pruned to "
                << event.particles().size()
                << " particles and " << event.vertices().size()
                << " vertices" << std::endl;
}

//====================================================================
void print(const HepMC3::GenEvent& event)
{
  for (auto particle : event.particles()) {
    HepMC3::ConstGenVertexPtr prod = particle->production_vertex();
    std::cerr << "Particle " << particle->id() << " ";
    if (prod)  std::cerr << prod->id();
    else       std::cerr << "none";
    std::cerr << std::endl;
  }
}

//====================================================================
template <typename Reader>
bool run(int argc, char** argv)
{
  std::string inFileName  = "-";
  std::string outFileName = "-";
  int         verb        = 0;
  int         max         = -1;
  bool        pass        = false;
  for (int i = 1; i < argc; i++) {
    std::string arg(argv[i]);
    if (arg == "-h" or arg == "--help") {
      std::cout << "Usage: " << argv[0] << " [OPTIONS]\n\n"
                << "Options:\n"
                << "  -i INPUT   Read input from file rather than stdin\n"
                << "  -o OUTPUT  Write output to file rather than stdout\n"
                << "  -n NEV     Process limited number of events\n"
                << "  -v         Set verbosity.  Each increases verbosity\n"
                << "  -p         Pass all particles through\n"
                << std::endl;
      return true;
    }

    if (arg[0] == '-') {
      switch (arg[1]) {
      case 'i':  inFileName  = argv[++i];            break;
      case 'o':  outFileName = argv[++i];            break;
      case 'v':  verb++;                             break;
      case 'n':  max         = std::stoi(argv[++i]); break;
      case 'p':  pass        = true;                 break;
      default:
        std::cerr << argv[0] << ": Unknown option " << arg << std::endl;
        return false;
      }
    } else {
      std::cerr << argv[0] << ": Unknown option " << arg << std::endl;
      return false;
    }
  }

  std::ifstream*      inPtr     = (inFileName == "-" //
                                   ? nullptr       //
                                   : new std::ifstream(inFileName.c_str()));
  std::ofstream*      outPtr    = (outFileName == "-" //
                                   ? nullptr        //
                                   : new std::ofstream(outFileName.c_str()));
  std::istream&       inStream  = (inPtr ? *inPtr : std::cin);
  std::ostream&       outStream = (outPtr ? *outPtr : std::cout);
  Reader              reader(inStream);
  HepMC3::WriterAscii writer(outStream);
  HepMC3::GenEvent    event;

  if (verb > 2)
    HepMC3::Setup::set_debug_level(30);

  auto stable = [](HepMC3::ConstGenParticlePtr particle) {
    switch (particle->status()) {
      case 1: // Final st
      case 2: // Decayed
      case 4: // Beam
        return true;
    }
    // if (particle->pid() == 9902210) return true;
    return false;
  };
  auto all    = [](HepMC3::ConstGenParticlePtr particle) { return true; };
  auto select = pass ? all : stable;
  auto more   = [max](int iev) { return max <= 0 or iev < max; };
  int  iev    = 0;
  while (more(iev) and reader.read_event(event) and not reader.failed()) {
    iev++;
    if (verb > 2) print(event);
    prune(event, select, verb);
    writer.write_event(event);
  }

  reader.close();
  writer.close();

  if (inPtr)  inPtr->close();
  if (outPtr) outPtr->close();

  return true;
}


#endif
// Local Variables:
//  clang-format-style: "none"
// End:
