// -*- mode: C++ -*-
//
// Copyright 2023 Christian Holm Christensen
//
// This software is distributed under the terms of the GNU General
// Public License v3 (GPL Version 3), copied verbatim in the file
// "COPYING".
//
// In applying this license CERN does not waive the privileges and
// immunities granted to it by virtue of its status as an
// Intergovernmental Organization or submit itself to any
// jurisdiction.
//
/*
  Compile with

    g++ -std=c++17 -I/opt/sw/inst/include pythia.cc \
    	-o pythia \
    	-L/opt/sw/inst/lib
    	-Wl,-rpath,/opt/sw/inst/lib
    	-lHepMC3
    	-lpythia8
*/
#include "Pythia.hh"

// ===================================================================
struct ppPythia : BasePythia
{
  virtual void configure()
  {
    // Beam parameter settings. Values below agree with default ones.
    _pythia.readString("Beams:idA                    = 2212");
    _pythia.readString("Beams:idB                    = 2212");
    _pythia.readString("Beams:eA                     = 7000.");
    _pythia.readString("Beams:eB                     = 7000.");
    _pythia.readString("Beams:eCM                    = 14000.");//As above
    _pythia.readString("SoftQCD:inelastic            = on");
    _pythia.readString("ParticleDecays:limitTau0     = on");
    _pythia.readString("ParticleDecays:tau0Max       = 10");
    _pythia.readString("Tune:ee                      = 7");
    _pythia.readString("Tune:pp                      = 14");
  }
};


int main(int argc, char** argv)
{
  return doMain<ppPythia>(argc, argv);
}
//
// EOF
//
