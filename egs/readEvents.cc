// -*- mode: C++ -*-
//
// Copyright 2023 Christian Holm Christensen
//
// This software is distributed under the terms of the GNU General
// Public License v3 (GPL Version 3), copied verbatim in the file
// "COPYING".
//
// In applying this license CERN does not waive the privileges and
// immunities granted to it by virtue of its status as an
// Intergovernmental Organization or submit itself to any
// jurisdiction.
//
/** Compile with

      g++ readEvents.cc -o readEvents -lHepMC3
*/
#include <iostream>
#include <cstdlib>
#include <sys/types.h> // POSIX only
#include <sys/stat.h>  // POISX only
#include <cstdio>
#include <thread>
#include <HepMC3/ReaderAscii.h>
#include <HepMC3/GenEvent.h>
#include <HepMC3/GenParticle.h>

struct Reader {
  using Impl        = HepMC3::ReaderAscii;
  using ImplPtr     = std::shared_ptr<Impl>;
  using Event       = HepMC3::GenEvent;
  using ParticlePtr = std::shared_ptr<HepMC3::GenParticle>;

  ImplPtr mImpl;
  
  Reader(const std::string& filename)
  {
    mImpl = std::make_shared<Impl>(filename);
  }
  bool nextEvent(Event& event)
  {
    mImpl->read_event(event);
    return not mImpl->failed();
  }
};
    

void usage(const std::string& progname, std::ostream& o=std::cout)
{
  o << "Usage: " << progname << " [OPTIONS]\n\n"
    << "Options:\n"
    << "  -e CMD_STRING   EG command string\n"
    << "  -h              Show this help\n\n"
    << "Any other options are sent to the EG command line\n"
    << std::endl;
}

int main(int argc, char** argv)
{
  using namespace std::chrono_literals;
  
  std::string egcmd  = "./egs/pythia";
  std::string egargs = "";
  for (int i = 1; i < argc; i++) {
    bool used = false;
    if (argv[i][0] == '-') {
      used = true;
      switch (argv[i][1]) {
      case 'e': egcmd = argv[++i]; break;
      case 'h': usage(argv[0]); return 0;
      default:  used = false; break;
      }
    }
    if (not used)
      egargs += std::string(" ") + argv[i];
  }

  // Make a temporary name
  std::string tmpName(std::tmpnam(nullptr));
  
  // Make a fifo
  int ret = mkfifo(tmpName.c_str(), 0600);
  if (ret != 0) 
    throw std::runtime_error("failed to make fifo "+tmpName);

  // Spawn EG which writes to fifo and put in background
  std::string cmd = egcmd+egargs+" -o "+tmpName+" &";
  std::cout << "EG command line is " << cmd << std::endl;
  ret = std::system(cmd.c_str());
  if (ret != 0) 
    throw std::runtime_error("failed to spawn "+egcmd);

  Reader reader(tmpName);
  Reader::Event event;
  
  while (reader.nextEvent(event)) {
    std::cout << "Event # " << event.event_number() << " with "
	      << event.particles().size() << std::endl;

    // Artificial sleep 
    if (event.particles().size() < 100)
      std::this_thread::sleep_for(500ms);
  }

  return 0;
}

	
