// -*- mode: C++ -*-
//
// Copyright 2023 Christian Holm Christensen
//
// This software is distributed under the terms of the GNU General
// Public License v3 (GPL Version 3), copied verbatim in the file
// "COPYING".
//
// In applying this license CERN does not waive the privileges and
// immunities granted to it by virtue of its status as an
// Intergovernmental Organization or submit itself to any
// jurisdiction.
//
/** Compile with

      g++ readEvents.cc -o readEvents -lHepMC3
*/
#include <iostream>
#include <cstdlib>
#include <sys/types.h> // POSIX only
#include <sys/stat.h>  // POISX only
#include <cstdio>
#include <thread>
#include <filesystem>
#include <TFile.h>
#include <TClonesArray.h>
#include <TChain.h>
#include <TParticle.h>

struct Reader {
  TClonesArray* mParticles = 0;
  TChain*       mTree = 0;
  int           mEntry = 0;
  std::filesystem::path mPath;
  
  Reader(const std::string& filename)
  {
    mParticles = new TClonesArray("TParticle");
    mTree      = new TChain("T");
    mTree->AddFile(filename.c_str());
    mTree->SetBranchAddress("Particles",&mParticles);
    mPath      = filename;
  }
  ~Reader()
  {
    if (mTree) {
      TFile* file = mTree->GetCurrentFile();
      if (file) mTree->RecursiveRemove(file);
      delete mTree;
    }
    std::error_code ec;
    std::filesystem::remove(mPath, ec);
  }
  void waitForContent()
  {
    std::cout << "Waiting for content" << std::endl;
    using namespace std::chrono_literals;

    // Wait until child process creates the file 
    while (not std::filesystem::exists(mPath))
      std::this_thread::sleep_for(500ms);

    // Wait until we have more data in the file than just the file
    // header
    while (std::filesystem::file_size(mPath) <= 256) 
      std::this_thread::sleep_for(500ms);

    // Give the child process 1 second to post the data to the file 
    std::cout << "Got it, sleeping a while" << std::endl;
    std::this_thread::sleep_for(1s);
  }
  const TClonesArray* nextEvent()
  {
    std::cout << "Trying to read event " << mEntry << std::endl;
    int ret = mTree->GetEntry(mEntry);
    std::cout << "Read " << ret << " bytes" << std::endl;
    if (ret < 0) {
      std::cerr << "Failed to read entry " << mEntry << std::endl;
      mEntry++;
    }
    if (ret <= 0) return nullptr;

    mTree->Refresh();
    mEntry++;
    return mParticles;
  }
};
    

void usage(const std::string& progname, std::ostream& o=std::cout)
{
  o << "Usage: " << progname << " [OPTIONS]\n\n"
    << "Options:\n"
    << "  -e CMD_STRING   EG command string\n"
    << "  -h              Show this help\n\n"
    << "Any other options are sent to the EG command line\n"
    << std::endl;
}

int main(int argc, char** argv)
{
  using namespace std::chrono_literals;
  
  std::string egcmd  = "./egs/tpythia";
  std::string egargs = "";
  for (int i = 1; i < argc; i++) {
    bool used = false;
    if (argv[i][0] == '-') {
      used = true;
      switch (argv[i][1]) {
      case 'e': egcmd = argv[++i]; break;
      case 'h': usage(argv[0]); return 0;
      default:  used = false; break;
      }
    }
    if (not used)
      egargs += std::string(" ") + argv[i];
  }

  // Make a temporary name
  std::string tmpName(std::tmpnam(nullptr));
  
  // Make a fifo
  // int ret = mkfifo(tmpName.c_str(), 0600);
  // if (ret != 0) 
  // throw std::runtime_error("failed to make fifo "+tmpName);

  // Spawn EG which writes to fifo and put in background
  std::string cmd = egcmd+egargs+" -o "+tmpName+" &";
  std::cout << "EG command line is " << cmd << std::endl;
  int ret = std::system(cmd.c_str());
  if (ret != 0) 
    throw std::runtime_error("failed to spawn "+egcmd);

  Reader reader(tmpName);
  reader.waitForContent();
  
  while (auto particles = reader.nextEvent()) {
    std::cout << "Event # " << reader.mEntry << " with "
	      << particles->GetEntries() << std::endl;

    // Artificial sleep 
    // if (particles->GetEntries() < 100)
    //   std::this_thread::sleep_for(500ms);
  }

  return 0;
}

	
