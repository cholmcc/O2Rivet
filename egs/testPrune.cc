#include "prune.hh"
#include <HepMC3/ReaderAscii.h>

int main(int argc, char** argv)
{
  return run<HepMC3::ReaderAscii>(argc, argv) ? 0 : 1;
}
// Local Variables:
//  clang-format-style: "none"
// End:
