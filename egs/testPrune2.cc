#include "prune.hh"
#include <HepMC3/ReaderAsciiHepMC2.h>

int main(int argc, char** argv)
{
  return run<HepMC3::ReaderAsciiHepMC2>(argc, argv) ? 0 : 1;
}
// Local Variables:
//  clang-format-style: "none"
// End:
