// -*- mode: C++ -*-
//
// Copyright 2023 Christian Holm Christensen
//
// This software is distributed under the terms of the GNU General
// Public License v3 (GPL Version 3), copied verbatim in the file
// "COPYING".
//
// In applying this license CERN does not waive the privileges and
// immunities granted to it by virtue of its status as an
// Intergovernmental Organization or submit itself to any
// jurisdiction.
//
/*
  Compile with

    g++ -std=c++17 -I/opt/sw/inst/include pythia.cc \
    	-o pythia \
    	-L/opt/sw/inst/lib
    	-Wl,-rpath,/opt/sw/inst/lib
	`root-config --cflags --libs` -lEG \
    	-lHepMC3
    	-lpythia8
*/
#include "Pythia.hh"
#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TParticle.h>

// ===================================================================
struct tppPythia : BasePythia
{
  int flush = 0;

  tppPythia() : BasePythia() { output = "pythia.root"; }
  
  virtual void usage(std::ostream& o=std::cout)
  {
    BasePythia::usage();
    o << "  -f PERIOD      Flush to disk every PERIOD event" << std::endl;
  }
  
  virtual bool parseOption(char option, char* arg)
  {
    if (BasePythia::parseOption(option, arg)) return true;
    switch (option) {
    case 'f': flush = std::stoi(arg); return true;
    default:  unknownOption(option); 
    }
    return false;
  }
  
  virtual void configure()
  {
    // Beam parameter settings. Values below agree with default ones.
    pythia.readString("Beams:idA                    = 2212");
    pythia.readString("Beams:idB                    = 2212");
    pythia.readString("Beams:eA                     = 7000.");
    pythia.readString("Beams:eB                     = 7000.");
    pythia.readString("Beams:eCM                    = 14000.");//As above
    pythia.readString("SoftQCD:inelastic            = on");
    pythia.readString("ParticleDecays:limitTau0     = on");
    pythia.readString("ParticleDecays:tau0Max       = 10");
    pythia.readString("Tune:ee                      = 7");
    pythia.readString("Tune:pp                      = 14");
  }

  bool fillParticles(TClonesArray* particles)
  {
    auto event   = pythia.event;
    
    
    for (int i = 1; i < event.size(); ++i) {
      auto& pyPart  = event[i];

      TParticle* particle = new((*particles)[i-1]) TParticle(pyPart.id(),
							     pyPart.statusHepMC(),
							     pyPart.mother1()-1,
							     pyPart.mother2()-1,
							     pyPart.daughter1()-1,
							     pyPart.daughter2()-1,
							     pyPart.px(),
							     pyPart.py(),
							     pyPart.pz(),
							     pyPart.e(),
							     pyPart.xProd(),
							     pyPart.yProd(),
							     pyPart.zProd(),
							     pyPart.tProd());
      particle->SetCalcMass(pyPart.m());
    }
    return true;
  }
  
  int loop()
  {
    TFile*        file      = TFile::Open(output.c_str(),"RECREATE");
    TTree*        tree      = new TTree("T","T");
    TClonesArray* particles = new TClonesArray("TParticle");
    tree->Branch("Particles",&particles);
    tree->SetDirectory(file);
    
    size_t nerr = 0;
    for (size_t iev = 0; iev < nev; iev++) {
      std::cerr << "Event # " << iev << std::endl;
      // Generate event.
      if (not makeEvent()) return 1;

      fillParticles(particles);
      tree->Fill();

      if (flush > 0 and (iev % flush == 0) and iev != 0) 
	tree->AutoSave("SaveSelf FlushBaskets Overwrite");
    }
    file->Write();
    file->Close();

    return 0;
  }
};


int main(int argc, char** argv)
{
  return doMain<tppPythia>(argc, argv);
}
//
// EOF
//
