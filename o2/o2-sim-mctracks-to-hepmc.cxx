// -*- mode: C++ -*-
//
// Copyright 2023 CERN and copyright holders of ALICE O2.
// See https://alice-o2.web.cern.ch/copyright for details of the copyright holders.
// All rights not expressly granted are reserved.
//
// This software is distributed under the terms of the GNU General Public
// License v3 (GPL Version 3), copied verbatim in the file "COPYING".
//
// In applying this license CERN does not waive the privileges and immunities
// granted to it by virtue of its status as an Intergovernmental Organization
// or submit itself to any jurisdiction.
//
#include <Framework/runDataProcessing.h>
#include <Framework/AnalysisHelpers.h>
#include <Framework/AnalysisTask.h>
#include <Generators/AODToHepMC.h>

namespace o2
{
namespace rivet
{
/** Tasks to convert AOD MC event information (without auxiliary
information) to a HepMC event structure.  */
struct Task {
  /** Type of converter */
  using Converter = o2::eventgen::AODToHepMC;
  /** Our converter */
  Converter mConverter;

  /** @{
@name Container types */
  using Header = Converter::Header;
  using Tracks = Converter::Tracks;
  /** @} */

  /** Initialize the job */
  void init(o2::framework::InitContext& ic)
  {
    mConverter.init();
  }
  /** Process events */
  void process(Header const& collision,
               Tracks const& particles)
  {
    mConverter.process(collision, particles);
  }
};
} // namespace rivet
} // namespace o2

using WorkflowSpec = o2::framework::WorkflowSpec;
using TaskName = o2::framework::TaskName;
using DataProcessorSpec = o2::framework::DataProcessorSpec;
using ConfigContext = o2::framework::ConfigContext;

WorkflowSpec defineDataProcessing(ConfigContext const& cfg)
{
  using o2::framework::adaptAnalysisTask;

  return WorkflowSpec{
    adaptAnalysisTask<o2::rivet::Task>(cfg,
                                       TaskName{"o2-sim-mctracks-to-hepmc"})};
}
//
// EOF
//
