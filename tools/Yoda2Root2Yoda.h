// -*- mode: C++ -*-
#include <iostream>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <TGraph2DErrors.h>
#include <TGraph2DAsymmErrors.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TProfile3D.h>
#include <YODA/Counter.h>
#include <YODA/Scatter1D.h>
#include <YODA/Scatter2D.h>
#include <YODA/Scatter3D.h>
#include <YODA/Histo1D.h>
#include <YODA/Histo2D.h>
#include <YODA/Profile1D.h>
#include <YODA/Profile2D.h>
//
//
// Rivet type        | ROOT type 
// ------------------+---------------------------------
// Counter           | TH1D (1 bin)
// Scatter1D         | TGraphAsymmErrors (points at index)
// Scatter2D         | TGraphAsymmErrors
// Scatter3D         | TGraph2DAsymmErrors
// Histo1D           | TH1D
// Histo2D           | TH2D
// -                 | TH3D
// Profile1D         | TProfile1D
// Profile2D         | TProfile2D
// -                 | TProfile3D
//
//====================================================================
TH1* fromCounter(const YODA::Counter& c)
{
  TH1* h = new TH1D(c.path().c_str(), c.title().c_str(),1, -.5, .5);
  h->SetEntries(c.numEntries());
  h->SetBinContent(1, c.sumW());
  return h;
}
//--------------------------------------------------------------------
std::shared_ptr<YODA::Counter> asCounter(TH1* h)
{
  assert(h->GetDimension() == 1);
  assert(h->GetXaxis()->GetNbins() == 1);

  auto c = std::make_shared<YODA::Counter>(h->GetName(),h->GetTitle());
  int  n = h->GetEntries();
  for (int i = 0; i < n; i++) c->fill(h->GetBinContent(1)/n);

  return c;
}
//====================================================================
TGraphAsymmErrors* fromScatter1D(const YODA::Scatter1D& s)
{
  TGraphAsymmErrors* g = new TGraphAsymmErrors(s.numPoints());
  g->SetName(s.path().c_str());
  g->SetTitle(s.title().c_str());

  for (size_t i = 0; i < s.numPoints(); i++) {
    auto& p = s.point(i);
    g->SetPoint     (i,i,p.x());
    g->SetPointError(i,0,0,p.xErrs().first,p.xErrs().second);
  }
  return g;
}
//--------------------------------------------------------------------
std::shared_ptr<YODA::Scatter1D> asScatter1D(TGraphAsymmErrors* g)
{
  auto s = std::make_shared<YODA::Scatter1D>(g->GetName(),g->GetTitle());
  for (size_t i = 0; i < g->GetN(); i++) {
    s->addPoint(g->GetY()[i],
		g->GetEYlow()[i],
		g->GetEYhigh()[i]);
  }
  return s;
}
//--------------------------------------------------------------------
std::shared_ptr<YODA::Scatter1D> asScatter1D(TGraphErrors* g)
{
  auto s = std::make_shared<YODA::Scatter1D>(g->GetName(),g->GetTitle());
  for (size_t i = 0; i < g->GetN(); i++) {
    s->addPoint(g->GetY()[i],g->GetEY()[i]);
  }
  return s;
}
//====================================================================
TGraphAsymmErrors* fromScatter2D(const YODA::Scatter2D& s)
{
  TGraphAsymmErrors* g = new TGraphAsymmErrors(s.numPoints());
  g->SetName(s.path().c_str());
  g->SetTitle(s.title().c_str());

  for (size_t i = 0; i < s.numPoints(); i++) {
    auto& p = s.point(i);
    g->SetPoint     (i,p.x(),p.y());
    g->SetPointError(i,
		     p.xErrs().first,p.xErrs().second,
		     p.yErrs().first,p.yErrs().second);
  }
  return g;
}
//--------------------------------------------------------------------
std::shared_ptr<YODA::Scatter2D> asScatter2D(TGraphAsymmErrors* g)
{
  auto s = std::make_shared<YODA::Scatter2D>(g->GetName(),g->GetTitle());
  for (size_t i = 0; i < g->GetN(); i++) {
    s->addPoint(g->GetX()[i],g->GetY()[i],
		g->GetEXlow()[i],
		g->GetEXhigh()[i],
		g->GetEYlow()[i],
		g->GetEYhigh()[i]);
  }
  return s;
}
//--------------------------------------------------------------------
std::shared_ptr<YODA::Scatter2D> asScatter2D(TGraphErrors* g)
{
  auto s = std::make_shared<YODA::Scatter2D>(g->GetName(),g->GetTitle());
  for (size_t i = 0; i < g->GetN(); i++) {
    s->addPoint(g->GetX()[i],g->GetY()[i],
		g->GetEX()[i],g->GetEY()[i]);
  }
  return s;
}
//====================================================================
TGraph2DAsymmErrors* fromScatter3D(const YODA::Scatter3D& s)
{
  TGraph2DAsymmErrors* g = new TGraph2DAsymmErrors(s.numPoints());
  g->SetName(s.path().c_str());
  g->SetTitle(s.title().c_str());

  for (size_t i = 0; i < s.numPoints(); i++) {
    auto& p = s.point(i);
    g->SetPoint     (i,p.x(),p.y(),p.z());
    g->SetPointError(i,
		     p.xErrs().first,p.xErrs().second,
		     p.yErrs().first,p.yErrs().second,
		     p.zErrs().first,p.zErrs().second);
  }
  return g;
}
//--------------------------------------------------------------------
std::shared_ptr<YODA::Scatter3D> asScatter3D(TGraph2DAsymmErrors* g)
{
  auto s = std::make_shared<YODA::Scatter3D>(g->GetName(),g->GetTitle());
  for (size_t i = 0; i < g->GetN(); i++) {
    s->addPoint(g->GetX()[i],g->GetY()[i],g->GetZ()[i],
		g->GetEXlow()[i],
		g->GetEXhigh()[i],
		g->GetEYlow()[i],
		g->GetEYhigh()[i],
		g->GetEZlow()[i],
		g->GetEZhigh()[i]);
  }
  return s;
}
//--------------------------------------------------------------------
std::shared_ptr<YODA::Scatter3D> asScatter3D(TGraph2DErrors* g)
{
  auto s = std::make_shared<YODA::Scatter3D>(g->GetName(),g->GetTitle());
  for (size_t i = 0; i < g->GetN(); i++) {
    s->addPoint(g->GetX()[i],g->GetY()[i],g->GetZ()[i],
		g->GetEX()[i],g->GetEY()[i],g->GetEZ()[i]);
  }
  return s;
}
//====================================================================
std::vector<double> TAxisEdges(const TAxis& a)
{
  std::vector<double> edges(a.GetNbins()+1);
  auto ar = a.GetXbins();
  if (ar->GetArray()) {
    for (size_t i = 0; i < ar->GetSize(); i++)
      edges[i] == (*ar)[i];
  }
  else {
    edges[0] = a.GetBinLowEdge(1);
    for (size_t i = 0; i < a.GetNbins(); i++)
      edges[i+1] == a.GetBinUpEdge(i+1);
  }
  return edges;
}
//====================================================================
TH1* fromHisto1D(const YODA::Histo1D& yh)
{
  TH1* h = new TH1D(yh.path().c_str(), yh.title().c_str(),
		    yh.numBinsX(),&(yh.xEdges()[0]));
  h->SetEntries(yh.numEntries());
  for (size_t i = 0; i < yh.numBinsX(); i++)
    // The area of a bin in YODA is sum of weights
    h->SetBinContent(i+1, yh.bin(i).area());

  return h;
}  
//--------------------------------------------------------------------
std::shared_ptr<YODA::Histo1D> asHisto1D(TH1* rh)
{
  auto h = std::make_shared<YODA::Histo1D>(TAxisEdges(*rh->GetXaxis()),
					   rh->GetName(),rh->GetTitle());
  for (size_t i = 0; i < rh->GetXaxis()->GetNbins(); i++) {
    auto& b = h->bin(i);
    auto  w = rh->GetBinContent(i+1);
    auto  x = rh->GetXaxis()->GetBinCenter(i+1);
    b.dbn() = YODA::Dbn1D(w,w,w*w,w*x,w*x*x);
  }
  return h;
}
//====================================================================
TH2* fromHisto2D(const YODA::Histo2D& yh)
{
  TH2* h = new TH2D(yh.path().c_str(), yh.title().c_str(),
		    yh.numBinsX(),&(yh.xEdges()[0]),
		    yh.numBinsY(),&(yh.yEdges()[0]));
  h->SetEntries(yh.numEntries());
  for (size_t i = 0; i < yh.numBinsX(); i++)
    for (size_t j = 0; j < yh.numBinsY(); j++)
      // The area of a bin in YODA is sum of weights
      h->SetBinContent(i+1, j+1, yh.bin(j * yh.numBinsX() + i).area());

  return h;
}
//--------------------------------------------------------------------
std::shared_ptr<YODA::Histo2D> asHisto2D(TH2* rh)
{
  assert(rh->GetDimension() == 2);
  auto h = std::make_shared<YODA::Histo2D>(TAxisEdges(*rh->GetXaxis()),
					   TAxisEdges(*rh->GetYaxis()),
					   rh->GetName(),rh->GetTitle());
  for (size_t i = 0; i < rh->GetXaxis()->GetNbins(); i++) {
    for (size_t j = 0; j < rh->GetXaxis()->GetNbins(); j++) {
      auto& b = h->bin(j * rh->GetXaxis()->GetNbins() + i);
      auto  w = rh->GetBinContent(i+1,j+1);
      auto  x = rh->GetXaxis()->GetBinCenter(i+1);
      auto  y = rh->GetYaxis()->GetBinCenter(j+1);
      b.dbn() = YODA::Dbn2D(w,w,w*w,
			    w*x,w*x*x,
			    w*y,w*y*y,
			    w*x*y);
    }
  }
  return h;
}
//====================================================================
void* asHisto3D(TH3* h)
{
  throw std::runtime_error("TH3 not supported by YODA");
  return 0;
}
//====================================================================
TProfile* fromProfile1D(const YODA::Profile1D& yh)
{
  TProfile* h = new TProfile(yh.path().c_str(), yh.title().c_str(),
			     yh.numBinsX(),
			     &(yh.xEdges()[0]));
  h->SetEntries(yh.numEntries());
  for (size_t i = 0; i < yh.numBinsX(); i++) {
    h->SetBinEntries(i+1, yh.bin(i).numEntries());
    h->SetBinContent(i+1, yh.bin(i).sumW());
  }
  return h;
}
//--------------------------------------------------------------------
std::shared_ptr<YODA::Profile1D> asProfile1D(TProfile* rh)
{
  auto h = std::make_shared<YODA::Profile1D>(TAxisEdges(*rh->GetXaxis()),
					     rh->GetName(),rh->GetTitle());
  for (size_t i = 0; i < rh->GetXaxis()->GetNbins(); i++) {
    auto& b = h->bin(i);
    auto  n = rh->GetBinEntries(i+1);
    auto  y = rh->GetBinContent(i+1);
    auto  w = y*n;
    auto  x = rh->GetXaxis()->GetBinCenter(i+1);
    b.dbn() = YODA::Dbn2D(n,w,w*w,
			  w*x,w*x*x,
			  w*y,w*y*y,
			  w*x*y);
  }
  return h;
}
//====================================================================
TProfile2D* fromProfile2D(const YODA::Profile2D& yh)
{
  TProfile2D* h = new TProfile2D(yh.path().c_str(), yh.title().c_str(),
				 yh.numBinsX(),
				 &(yh.xEdges()[0]),
				 yh.numBinsY(),
				 &(yh.yEdges()[0]));
  h->SetEntries(yh.numEntries());
  for (size_t i = 0; i < yh.numBinsX(); i++)
    for (size_t j = 0; j < yh.numBinsY(); j++){
      h->SetBinEntries(i+1, yh.bin(i).numEntries());
      h->SetBinContent(i+1, yh.bin(i).sumW());
    }
  return h;
}
//--------------------------------------------------------------------
std::shared_ptr<YODA::Profile2D> asProfile2D(TProfile2D* rh)
{
  assert(rh->GetDimension() == 2);
  auto h = std::make_shared<YODA::Profile2D>(TAxisEdges(*rh->GetXaxis()),
					     TAxisEdges(*rh->GetYaxis()),
					     rh->GetName(),rh->GetTitle());
  for (size_t i = 0; i < rh->GetXaxis()->GetNbins(); i++) {
    for (size_t j = 0; j < rh->GetXaxis()->GetNbins(); j++) {
      auto& b = h->bin(j * rh->GetXaxis()->GetNbins() + i);
      auto  z = rh->GetBinContent(i+1,j+1);
      auto  n = rh->GetBinEntries(rh->GetBin(i+1,j+1));
      auto  w = z*n;
      auto  x = rh->GetXaxis()->GetBinCenter(i+1);
      auto  y = rh->GetYaxis()->GetBinCenter(j+1);
      b.dbn() = YODA::Dbn3D(n,w,w*w,
			    w*x,w*x*x,
			    w*y,w*y*y,
			    w*z,w*z*z,
			    w*x*y,w*x*z,w*y*z);
    }
  }
  return h;
}
//====================================================================
void* asProfile3D(TProfile3D* h)
{
  throw std::runtime_error("TH3 not supported by YODA");
  return 0;
}
//====================================================================

    
    
