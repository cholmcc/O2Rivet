#!/bin/bash
#
# A example workflow MC->RECO->AOD for a simple pp production
# excluding ZDC
NWORKERS=${NWORKERS:-6}
SIMENGINE=${SIMENGINE:-TGeant4}

${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py \
             -eCM 14000 \
             -col pp \
             -gen pythia8 \
             -proc "cdiff" \
             -tf 2  \
             -ns 50 \
             -e ${SIMENGINE} \
             -j ${NWORKERS} \
             -run 303000 \
             -seed 624 \
             -interactionRate 50000 

${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json -tt aod
