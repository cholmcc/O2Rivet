#include "zip.h"
#include <iostream>
#include <vector>
#include <valarray>
#include <list>

template <typename T1, typename T2>
void test2(const T1& t1, const T2& t2)
{
  for (auto&& [v1,v2] : zip::zip(t1,t2)) 
    std::cout << v1 << ", " << v2 << std::endl;
}
template <typename T1, typename T2, typename T3>
void test3(const T1& t1, const T2& t2, const T3& t3)
{
  for (auto&& [v1,v2,v3] : zip::zip(t1,t2,t3)) 
    std::cout << v1 << ", " << v2 << ", " << v3 << std::endl;
}

template <typename T1, typename T2>
void test2t(const T1& t1, const T2& t2)
{
  for (auto&& v : zip::zip(t1,t2)) 
    std::cout << std::get<0>(v) << ", " << std::get<1>(v) << std::endl;
}

int
main()
{
  std::vector<int> a{0,1,2,3};
  std::vector<int> b{10,11,12,13};
  std::valarray<double> c{.1,.2,.3,.4};
  std::valarray<bool> d{false,true,false};
  std::vector<bool> e{true,true,false};

  test2(a,b);
  test2(a,c);
  test2(b,d);
  test2(d,e);

  test3(a,b,c);
  test3(c,d,e);

  test2t(a,b);
  
  return 0;
}
