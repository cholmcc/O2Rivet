#!/usr/bin/env python

from pprint import pprint 

def createList(field):
    ch = lambda f : len(f) > 0 and int(f) >= 0
    l  = list(set([int(f) for f in field[2:].split(',') if ch(f)]))
    l.sort()
    return l
    
def createTrack(line):
    fields         = line.split()
    mIds           = fields[3][2:].split(',')
    dIds           = fields[4][2:].split(',')

    d = {
        'id':     int(fields[0]),
        'pdg':    int(fields[1]),
        'status': int(fields[2]),
        'prod':   [float(f) for f in fields[5].split(',')],
        'mids':   createList(fields[3]),
        'dids':   createList(fields[4]),
    }

    # if len(d['dids']) > 2:
    #     print(f'More than 2 daughters: {d["dids"]}')
    # 
    # if len(d['dids']) == 2:
    #     first = d['dids'][0]
    #     last  = d['dids'][1]
    #     if abs(last-first) != 1:
    #         print(f'Daughter range: {d["dids"]}')
        
    return d;

def createParticle(track):
    return { 'id':    track['id'],
             'pdg':   track['pdg'],
             'status':track['status'],
             'start': None,
             'end':   None }
        
def createVertex(x,y,z,t):
    return {'incoming': [],
            'outgoing': [],
            'position': [x,y,z,t]
            }

def setProduction(particle,vertex):
    if vertex is None:
        print(f'No production vertex')
        return

    particle['start'] = vertex
    vertex['outgoing'].append(particle)

def setEnd(particle,vertex):
    if vertex is None:
        print(f'No end vertex')
        return

    particle['end'] = vertex
    vertex['incoming'].append(particle)
    

def parse(filename='foo.dat.0001'):
    tracks = {}

    with open(filename) as inFile:
        lineno = 0
        while inFile:
            line = inFile.readline()
            lineno += 1
            # print(f'{lineno:6d}: "{line}"')
            if line.startswith('Event'):
                continue
            if len(line) <= 0:
                break 
        
            track               = createTrack(line)
            tracks[track['id']] = track


    return tracks

def buildRecursive(tid,tracks,particles,missing):
    if tid in particles:
        return particles[tid]

    if tid not in tracks:
        missing.add(tid)
        return None
    
    track          = tracks[tid]
    motherIds      = track['mids']
    daughterIds    = track['dids']
    particle       = createParticle(track)
    particles[tid] = particle

    mothers        = []
    prodVtx        = None
    for mid in motherIds:
        mother = buildRecursive(mid, tracks, particles, missing)
        if mother is None:
            continue
        
        mothers.append(mother)
        if prodVtx is None:
            prodVtx = mother['end']
        if prodVtx is not None and not prodVtx is mother['end']:
            print(f'Mother {mid} gives a different vertex than previous')

    if prodVtx is None and particle['status'] != 4 and len(mothers)>0:
        prodVtx = createVertex(*track['prod'])

        for mother in mothers:
            if mother['end'] is None:
                if mother['start'] is prodVtx:
                    print('Trying to set mother end to start')
                else:
                    setEnd(mother, prodVtx)
                if not mother['end'] is prodVtx:
                    print(f'Mother inconsistent end vertex')

    if prodVtx:
        setProduction(particle, prodVtx)


    return particle

def fmtVtx(vertex):
    inc = ",".join([str(p['id']) for p in vertex['incoming']])
    out = ",".join([str(p['id']) for p in vertex['outgoing']])
    pos = ",".join([str(c) for c in vertex['position']])
    return f'{pos:36s} in={inc:16} out={out:16}'

def fmtList(l):
    return ",".join([str(e) for e in l])


def build(tracks):
    particles = {}
    missing   = set()
    for tid in tracks:
        buildRecursive(tid,tracks,particles,missing)


    if len(missing) > 0:
        l = list(missing)
        l.sort()
        print(f'Missing tracks: {missing}')
        
    return particles

def fleshoutParticle(tid,tracks,particles,missing):
    track       = tracks   [tid]
    particle    = particles[tid]
    endVtx      = particle['end']
    daughterIds = track['dids']
    candidate   = endVtx
    headless    = []
    
    for did in daughterIds:
        if did not in particles:
            missing.add(did)
            continue
            
        dtrack   = tracks[did]
        daughter = particles[did]
        prodVtx  = daughter['start']

        if prodVtx is None:
            headless.append(daughter)
            continue

        inc    = dtrack['mids']
        mrange = len(inc) == 2 and abs(inc[0]-inc[1]) > 1
            
        if not mrange and particle not in prodVtx['incoming']:
            print(f'Mother {tid} not in production vertex of '+
                  f'daughter {did}'+'\n'+
                  f'  daug={fmtList(daughterIds)}'+'\n'
                  f'  prod={fmtVtx(prodVtx)}')
            if endVtx is not None and daughter in endVtx['outgoing']:
                print(f'Removing daughter {did} from end vertex'+'\n'+
                      f'  {fmtVtx(endVtx)}')
            # if no end vertex is currently set or the daughter isn't
            # registered there, then do nothing.
            continue
        
        if endVtx is not None and \
           not prodVtx is endVtx:
            print(f'Production vertex of {did} does not match '
                  f'end vertex of {tid}'+'\n'
                  f'  end ={fmtVtx(endVtx)}'+'\n'
                  f'  prod={fmtVtx(prodVtx)}'+'\n'
                  f'  daug={fmtList(daughterIds)}'+'\n'
                  f'  moth={fmtList(dtrack["mids"])}')
        if candidate is not None and \
           not candidate is prodVtx:
            print(f'Candidate vertex of {did} from {tid} does not match '
                  f'previously found'+'\n'
                  f'  cand={fmtVtx(candidate)}'+'\n'
                  f'  prod={fmtVtx(prodVtx)}'+'\n'
                  f'  daug={fmtList(daughterIds)}'+'\n'
                  f'  moth={fmtList(dtrack["mids"])}')

        candidate = prodVtx


    if not endVtx:
        if not candidate and particle['status'] in [4,2]:
            print(f'Particle {tid} w/status = {particle["status"]} does '
                  f'not have an end vertex, nor was any found')

        if candidate:
            endVtx = candidate
            setEnd(particle,candidate)
    
    if len(headless) > 0:
        print(f'Found {len(headless)} headless daughters of {tid} '
              f'{[d["id"] for d in headless]} '
              f'{"and no end vertex found" if endVtx is None else ""}')
        if endVtx is not None:
            for daughter in headless:
                setProduction(daughter, endVtx)

    return particle if  particle['status']==4 else None
            
    
def fleshout(tracks,particles):
    beams = []
    missing = set()
    for tid in tracks:
        beam = fleshoutParticle(tid,tracks,particles,missing)
        if beam:
            beams.append(beam)

    if len(missing) > 0:
        l = list(missing)
        l.sort()
        print(f'Missing daughters: {l}')
        
    return beams


def checkCycle(prodVtx,particle,history):
    if prodVtx is None:
        return None
    print(history)
    
    for parent in prodVtx['incoming']:
        if particle is parent:
            return history+[parent["id"]]
        
        pprodVtx = parent['start']
        chk      = checkCycle(prodVtx=pprodVtx,
                              particle=particle,
                              history=history)
        if chk is not None:
            return chk

    return None

def checkCycles(particles):
    ncycles = 0
    for no, particle in particles.items():
        chk = checkCycle(particle['start'],
                         particle,
                         [particle["id"]])
        
        if chk:
            ncycles += 1
            print(f'Found cycle: {chk}')
        
        

    print(f'Found a total of {ncycles}')

def printParticleTree(particle,ind,depth,seen):
    endVtx  = particle['end']
    inc     = [str(p["id"]) for p in endVtx['incoming']] \
        if endVtx is not None and particle['status'] != 2 else []
    print(f'{ind}{particle["id"]} '
          f'({particle["pdg"]},{particle["status"]}) '
          f'[{",".join(inc)}]')

    if endVtx is None:
        return

    if endVtx in seen:
        return

    seen.append(endVtx)
    
    if depth >= 0 and len(ind) > depth:
        return

    for daughter in endVtx['outgoing']:
        printParticleTree(daughter,ind+" ")
        
def printTree(beams,depth=100):
    seen = []
    for beam in beams:
        printParticleTree(beam, "", depth,seen)
        
if __name__ == '__main__':
    from argparse import ArgumentParser

    ap = ArgumentParser('test event')
    ap.add_argument('input',nargs='?',default='foo.dat.0001',
                    help='Input file')
    ap.add_argument('-d','--depth',type=int,default=-1,
                    help='Max depth to print tree')
    ap.add_argument('-p','--print',action='store_true',dest='print',
                    help='Print tree')
    ap.add_argument('-c','--cycles',action='store_true',dest='check',
                    help='Check for cycles')
    args = ap.parse_args()

    tracks    = parse(args.input)
    particles = build(tracks)
    beams     = fleshout(tracks,particles)
    if args.print:
        printTree(beams,args.depth)
    if args.check:
        checkCycles(particles)
    
    # pprint(particles,depth=3)
