#include "Yoda2Root2Yoda.h"
#include <random>
#include <valarray>
#include "zip.h"

using observations1d = std::valarray<double>;
using weights = std::valarray<double>;

std::shared_ptr<YODA::Histo1D> makeHisto1D(const std::string&    name,
					   const std::string&    title,
					   const observations1d& obs,
					   const weights&        weights)
{
  double xMin = obs.min();
  double xMax = obs.max();
  size_t n    = 10;
  
  auto h = std::make_shared<YODA::Histo1D>(n,xMin,xMax,name,title);
  for (auto&& [x,w] : zip::zip(obs,weights)) h->fill(x,w);

  return h;
}

template <typename Generator>
void generate(Generator generator, std::valarray<double>& out)
{
  std::generate(std::begin(out),std::end(out),generator);
}

int main()
{
  std::random_device                     seed_device;
  std::default_random_engine             engine(seed_device());
  std::normal_distribution<double>       normal(0,1);
  std::uniform_real_distribution<double> uniform(0,1);

  observations1d obs(100);
  observations1d weights(100);

  generate([&engine,&normal] (){return normal(engine); }, obs);
  generate([&engine,&uniform](){return uniform(engine); },weights);

  auto histo1d = makeHisto1D("h1","h1",obs,weights);
  auto h1      = fromHisto1D(*histo1d);
  
  return 0;
}

