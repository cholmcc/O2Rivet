// -*- mode: C++ -*-
#ifndef zip_zip_h
#define zip_zip_h
#include <type_traits>
#include <utility>
#include <tuple>
#include <vector>

namespace zip {
  namespace details {
    //================================================================
    /**
     * Select the used iterator type.  If the passed type is const,
     * then return a const iterator.
     */
    template <typename T>
    using select_iterator_for = std::conditional_t<
      std::is_const_v<std::remove_reference_t<T>>,
      decltype(std::begin(std::declval<std::decay_t<T>>())),
      decltype(std::cbegin(std::declval<std::decay_t<T>>()))>;
    
    //----------------------------------------------------------------
    /**
     * We want to get back a reference type in most cases.  However,
     * since std::vector<bool> is typically implemented as a bit
     * field, we need to take special care of that.
     */
    template <typename I>
    using select_access_type_for = std::conditional_t<
      std::is_same_v<I, std::vector<bool>::iterator> ||
      std::is_same_v<I, std::vector<bool>::const_iterator>,
      typename std::iterator_traits<I>::value_type,
      typename std::iterator_traits<I>::reference>;

    //================================================================
    /**
     * Implements an actual check if any two elements of the lhs and
     * rhs tuple arguments match.  The index sequence is used to
     * unfold the parameter pack.
     */
    template <typename ... T, std::size_t ... I>
    bool any_match_impl(std::tuple<T...> const & lhs,
			std::tuple<T...> const & rhs,
			std::index_sequence<I...>) {
      return (... | (std::get<I>(lhs) == std::get<I>(rhs)));
    }


    //----------------------------------------------------------------
    /**
     * Check if any pair of elements of the lhs and rhs tuples match.
     */
    template <typename ... T>
    bool any_match(std::tuple<T...> const & lhs, 
		   std::tuple<T...> const & rhs) {
      return any_match_impl(lhs, rhs, std::index_sequence_for<T...>{});
    }
    
    //================================================================
    /**
     * The type of iterator used by the zipper.
     */
    template <typename ...I>
    struct iterator
    {
      /** Define what the acces type for each element in the tuple
       * should be. Mostly these will be references. */
      using value_type = std::tuple<select_access_type_for<I>...>;
      
      /** No default constructor */
      iterator() = delete;
      
      /** Constructor from a set of iterators.  This is forwarded to the
       * tuple member to get ownership. */
      iterator(I... iterators)
	: m_iterators{std::forward<I>(iterators)...}
      {}
      
      /** Pre-increment iterator.  This applies a increment to all the
       * stored iterators */ 
      iterator& operator++() {
	std::apply([](auto&& ... iterators)  {
	  ((iterators += 1), ...); }, m_iterators);
	return *this;
      }

      /** Post-increment iterator. Makes a temporary copy of this,
       * increments this, and then returns temporary.  */
      iterator operator++(int) {
	auto tmp = *this;
	++*this;
	return tmp;
      }

      /** Compare two iterators.  Note that if _any_ pair of elements from
       * this and other iterator match, then this returns true. */
      bool operator==(const iterator& i) {
	return any_match(m_iterators,i.m_iterators); }
      
      
      /** Compare two iterators.  Note that if _no_ pair of elements from
       * this and other iterator match, then this returns true. */
      bool operator!=(const iterator& i) {
	return not (*this == i);
      }
      
      /** Deference the iterator */
      value_type operator*() {
	return std::apply([](auto&& ... i) { return value_type(*i...); },
			  m_iterators);
      }
      
    protected:
      /** Our iterators */
      std::tuple<I...> m_iterators;
    };

  
    //================================================================
    /**
     * Structure to define a zipper to iterate over.
     */
    template <typename ...T>
    struct zipper
    {
      /** Deduce the kinds of iterators to use */
      using iterator_type = iterator<select_iterator_for<T>...>;

      /** Constructor from containers */
      template <typename ...Containers>
      zipper(Containers&& ... containers)
	: m_containers{std::forward<T>(containers)...}
      {}

      /** Get iterator pointing to start of set */
      iterator_type begin() {
	return std::apply([](auto&& ...containers) {
	  return iterator_type(std::begin(containers)...); },
	  m_containers);
      }

      /** Get iterator pointing to just beyond end of set */
      iterator_type end() {
	return std::apply([](auto&& ... containers) {
	  return iterator_type(std::end(containers)...); },
	  m_containers);
      }
    protected:
      /** Our set of containers */
      std::tuple<T...> m_containers;
    };
  }
  

  /** Construct a zipper object to iterate over */
  template <typename ...T>
  auto zip(T... containers)
  {
    return zip::details::zipper<T...>{std::forward<T>(containers)...};
  }
}
#endif

  
  
